'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./default');

const tmpPassword = sequelize.define('tmpPassword', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    uid: Sequelize.INTEGER,
    cid: Sequelize.INTEGER,
    tmpPassword: Sequelize.TEXT
});

module.exports = tmpPassword;
