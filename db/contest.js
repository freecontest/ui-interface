'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./default');

const Contest = sequelize.define('contests', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: Sequelize.TEXT,
    startTime: Sequelize.DATE,
    endTime: Sequelize.DATE,
    duration: Sequelize.INTEGER,
    writters: Sequelize.ARRAY(Sequelize.STRING(65)),
    id_contest: {
        type: Sequelize.INTEGER,
        defaultValue: 1
    },
    code: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    },
    practice_url: {
        type: Sequelize.TEXT
    },
    facebook_url: {
        type: Sequelize.TEXT
    }
});

module.exports = Contest;
