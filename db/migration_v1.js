'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return [
            queryInterface.addColumn(
                'contests',
                'practice_url', {
                    type: Sequelize.STRING,
                    allowNull: true
                }
            )
        ];
    },

    down: function (queryInterface, Sequelize) {
        return [
            queryInterface.removeColumn('contests', 'practice_url')
        ];
    }
};
