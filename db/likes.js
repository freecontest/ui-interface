'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./default');

const Like = sequelize.define('likes', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    uid: Sequelize.INTEGER,
    pid: Sequelize.INTEGER
});

module.exports = Like;
