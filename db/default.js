'use strict';

require('dotenv').config({
    silent: true
});

const dbURI = process.env.dbURI
const dbName = process.env.dbName
const username = process.env.dbUsername
const password = process.env.dbPassword

const Sequelize = require('sequelize');
const Op = Sequelize.Op
const operatorsAliases = {
    $eq: Op.eq,
    $ne: Op.ne,
    $gte: Op.gte,
    $gt: Op.gt,
    $lte: Op.lte,
    $lt: Op.lt,
    $not: Op.not,
    $in: Op.in,
    $notIn: Op.notIn,
    $is: Op.is,
    $like: Op.like,
    $notLike: Op.notLike,
    $iLike: Op.iLike,
    $notILike: Op.notILike,
    $regexp: Op.regexp,
    $notRegexp: Op.notRegexp,
    $iRegexp: Op.iRegexp,
    $notIRegexp: Op.notIRegexp,
    $between: Op.between,
    $notBetween: Op.notBetween,
    $overlap: Op.overlap,
    $contains: Op.contains,
    $contained: Op.contained,
    $adjacent: Op.adjacent,
    $strictLeft: Op.strictLeft,
    $strictRight: Op.strictRight,
    $noExtendRight: Op.noExtendRight,
    $noExtendLeft: Op.noExtendLeft,
    $and: Op.and,
    $or: Op.or,
    $any: Op.any,
    $all: Op.all,
    $values: Op.values,
    $col: Op.col
}

try {
    const sequelize = new Sequelize(
        `postgres://${username}:${password}@${dbURI}:5432/${dbName}`, {
            dialect: 'postgres',
            dialectOptions: {
                ssl: false
            },
            operatorsAliases,
            logging: false
        }
    );

    module.exports = sequelize;
} catch (e) {
    console.log(e)
}
