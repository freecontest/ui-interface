'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./default');

const User = sequelize.define('users', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username: {
        type: Sequelize.STRING(65),
        unique: true
    },
    password: Sequelize.STRING(65),
    name: Sequelize.TEXT,
    schoolName: Sequelize.TEXT,
    yob: Sequelize.INTEGER,
    color: {
        type: Sequelize.INTEGER,
        defaultValue: 1
    },
    rating: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    },
    avatar: {
        type: Sequelize.TEXT,
        defaultValue: './static/img/anonymous.jpg'
    },
    role: {
        type: Sequelize.TEXT,
        defaultValue: 'user'
    },
    showOnRanking: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    }
});

module.exports = User;
