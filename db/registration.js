'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./default');

const Registration = sequelize.define('registration', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    uid: Sequelize.INTEGER,
    cid: Sequelize.INTEGER
});

module.exports = Registration;
