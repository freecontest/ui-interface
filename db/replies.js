'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./default');

const Reply = sequelize.define('replies', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    uid: Sequelize.INTEGER,
    cid: Sequelize.INTEGER,
    content: Sequelize.TEXT
});

module.exports = Reply;
