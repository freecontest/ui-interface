'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./default');

const logsUpdateRating = sequelize.define('logsUpdateRating', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username: Sequelize.TEXT,
    rating: Sequelize.INTEGER,
    delta: Sequelize.INTEGER,
    rank: Sequelize.INTEGER,
    contestID: Sequelize.INTEGER,
    contestName: Sequelize.TEXT
});

module.exports = logsUpdateRating;
