'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./default');

const Post = sequelize.define('posts', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: Sequelize.TEXT,
    content: Sequelize.TEXT,
    author: Sequelize.STRING(65)
});

module.exports = Post;
