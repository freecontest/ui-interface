'use strict';

const Sequelize = require('sequelize');
const sequelize = require('./default');

const Comment = sequelize.define('comments', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    uid: Sequelize.INTEGER,
    pid: Sequelize.INTEGER,
    content: Sequelize.TEXT
});

module.exports = Comment;
