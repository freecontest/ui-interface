'use strict';

const path = require('path');
const view = require('consolidate');
const serveStatic = require('serve-static');
const session = require('express-session');
const qs = require('querystring');
const url = require('url');

// use .env
require('dotenv').config({
    silent: true
});

function validJSON(text) {
    try {
        JSON.parse(text);
    } catch (exception) {
        return false;
    }
    return true;
}

module.exports = (app) => {
    // html render
    app.use((req, res, next) => {
        res.render = function render(filename, params) {
            var file = path.resolve(__dirname, '..', 'views', filename);
            view.mustache(file, params || {}, function (err, html) {
                if (err) {
                    return next(err);
                }
                res.setHeader('Content-Type', 'text/html');
                res.end(html);
            });
        };

        next();
    });

    // parse form for method GET
    app.use((req, res, next) => {
        if (req.method !== 'GET') {
            return next();
        }
        req.query = url.parse(req.url, true).query;

        next();
    })

    // parse form for method POST & PUT
    app.use((req, res, next) => {
        if (req.method !== 'POST' && req.method !== 'PUT' &&
            req.method !== 'PATCH') {
            return next();
        }

        let body = '';
        req.on('data', (buffer) => {
            body = body + buffer.toString();
        });

        req.on('end', () => {
            if (validJSON(body)) {
                req.body = JSON.parse(body);
            } else {
                req.body = '';
            }
            next();
        });
    })

    // json render
    app.use((req, res, next) => {
        res.json = function (obj) {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(obj));
        };

        next();
    });

    // serve up static folder
    app.use('/static', serveStatic('./static'));

    //use session
    app.use(session({
        secret: process.env.secretSessionKey,
        resave: true,
        saveUninitialized: true
    }));
};
