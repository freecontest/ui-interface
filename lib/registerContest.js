'use strict'

require('dotenv').config()

const request = require('request')

function requestSync(opts) {
    return new Promise(function (resolve, reject) {
        request.post(opts, function (err, httpResponse, body) {
            if (err) return reject({
                err,
                httpResponse
            })
            resolve({
                httpResponse,
                body
            })
        })
    })
}

async function Register(user_name, password, first_name, last_name, contest_id, hidden) {
    // registration is fail with username has "."
    if (/^([A-Za-z0-9-_])*$/.test(user_name) === false) {
        return {
            err: 'Tên đăng nhập bạn của bạn không còn hợp lệ để đăng ký, vui lòng báo với TNV để được tư vấn khắc phục.'
        }
    }

    const {
        daemon_hostname,
        daemon_port,
        daemon_secret_key
    } = process.env

    // TODO: this is pretty hacky, since NodeJS doesn't allow building
    // a URL from scratch
    const url = `http://${daemon_hostname}:${daemon_port}/register`

    // registration daemon only allows numberic value for hidden
    hidden = hidden ? 0 : 1

    let opts = {
        url: url,
        form: {
            username: user_name,
            password: password,
            first_name: first_name,
            last_name: last_name,
            contest_id: contest_id,
            hidden: hidden,
            secret_key: daemon_secret_key
        }
    }
    let {
        httpResponse,
        body
    } = await requestSync(opts)
    return JSON.parse(body)
}

module.exports = Register
