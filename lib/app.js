'use strict';

// required
const http = require('http');
const Router = require('router');
const finalhandler = require('finalhandler');
const path = require('path');
const app = new Router();

// use middleware
require('./middleware')(app);

// default routes
app.all('/', require('../routes/default/index'));
app.all('/api/auth', require('../routes/default/auth'));
app.all('/api/contest/register', require('../routes/default/register'));
app.all('/api/users', require('../routes/default/users'));
app.all('/api/posts', require('../routes/default/posts'));
app.all('/api/actions', require('../routes/default/action'));
app.all('/api/tmpPassword', require('../routes/default/tmpPassword'));
app.all('/api/singleContest', require('../routes/default/contest'));
app.all('/api/regSingleContest', require('../routes/default/regContest'));
// admin routes
app.all('/api/contest', require('../routes/admin/contest')); // only method GET for user
app.all('/api/admin/users', require('../routes/admin/users.js'));

// html5 fix
app.use(require('../routes/default/index'));

// create server
const server = http.createServer();
server.on('request', (req, res) => {
    app(req, res, finalhandler(req, res));
});

module.exports = server;
