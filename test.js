const bcrypt = require('bcrypt-nodejs')

const hash = '$2a$10$K1kzx2TAX/DYCAdMq7V6qOhCeowI.oeR4w.2QXhoQ3tTwhZahsYfu'

console.log(bcrypt.compareSync("bacon", hash))