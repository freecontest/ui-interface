'use strict'

const fetch = require('node-fetch')
const uri = 'https://code.freecontest.xyz/ranking/scores'

const User = require('./db/user')
const Contest = require('./db/contest')
const logsUpdateRating = require('./db/logsUpdateRating')

async function system() {}
system.getRating = async function(username) {
    console.log('Get rating ' + username)
    let cond = {
        username: { $eq: username }
    }
    let contestant = await User.find({ where: cond })
    return contestant.rating
}

system.getColor = function(rating) {
    if (0    <= rating && rating <= 1199) return 2;
    if (1200 <= rating && rating <= 1399) return 3;
    if (1400 <= rating && rating <= 1599) return 4;
    if (1600 <= rating && rating <= 1899) return 5;
    if (1900 <= rating && rating <= 2199) return 6;
    if (2200 <= rating && rating <= 2299) return 7;
    if (2300 <= rating && rating <= 2399) return 8;
    if (2400 <= rating && rating <= 2599) return 9;
    if (2600 <= rating && rating <= 2899) return 10;
    if (2900 <= rating)                   return 11;
}

system.getLatestParty = async function() {
    let party = await Contest.findOne({
        limit: 1,
        order: [ ['createdAt', 'DESC'] ]
    })
    return party.dataValues
}

system.decodeUsername = function(s) {
    var username = s

    let remove = []
    for (var i = 0; i < username.length; ++i) {
        remove[i] = false
    }

    let result = ''

    for (var i = 0; i < username.length; ++i) {
        if (remove[i]) continue
        if (username[i] === '_') {
            result += unescape('%' + username[i + 1] + username[i + 2])
            remove[i + 1] = true
            remove[i + 2] = true
        } else {
            result += username[i]
        }
    }

    return result
}

system.updateRating = async function(party, contestant) {
    let cond = {
        username: { $eq: contestant.username }
    }

    if (contestant.rating === 0) contestant.rating = 1500

    let newRating = contestant.rating + contestant.delta

    await User.update({
        rating: newRating,
        color: system.getColor(newRating)
    }, { where: cond })

    await logsUpdateRating.create({
        username: contestant.username,
        rating: newRating,
        delta: contestant.delta,
        rank: contestant.rank,
        contestID: party.id,
        contestName: party.name
    })
}

system.updateRatings = async function(contestants) {
    let party = await system.getLatestParty()
    for (let contestant of contestants) {
        await system.updateRating(party, contestant)
        console.log('update success ' + contestant.username)
    }
}

/// SYSTEM FUNC END HERE

async function load() {
    let startFetch = new Date()
    console.log('Start fetch user & rank', startFetch)

    let scores = await fetch(uri).then(txt => txt.text())
    console.log(scores)
    return ;

    let ranking = []
    for (let user in scores) {
        let sum = 0
        for (let prob in scores[user]) {
            sum += Math.floor(scores[user][prob])
        }
        ranking.push({
            username: system.decodeUsername(user),
            totalScores: sum
        })
    }

    ranking.sort((a, b) => b.totalScores - a.totalScores)

    ranking[0].rank = 1
    for (let index = 1; index < ranking.length; ++index) {
        ranking[index].rank = (ranking[index].totalScores == ranking[index - 1].totalScores)
                            ? ranking[index - 1].rank
                            : index + 1
    }

    let contestants = []
    for (let i = 0; i < ranking.length; ++i) {
        let contestant = ranking[i]
        let rating = await system.getRating(contestant.username)

        contestants.push({
            username: contestant.username,
            rank: contestant.rank,
            points: contestant.totalScores,
            rating: rating,
            needRating: 0,
            seed: 0.0,
            delta: 0
        })
    }

    let startReCalculate = new Date()
    console.log('Fetched all in ' + (startReCalculate.getTime() - startFetch.getTime()).toString() + ' miliseconds.')
    console.log('Start ReCalculateRating', startReCalculate)

    let f = new ReCalculateRating(contestants)
    f.ReCalculateRating()

    contestants = f.contestants

    contestants.sort((a, b) => b.delta - a.delta)

    let finishedTime = new Date()
    console.log('Finished recalculate rating in ' + (finishedTime.getTime() - startReCalculate.getTime()).toString() + ' miliseconds.')
    console.log('Start update rating', finishedTime)

    await system.updateRatings(contestants)

    let finishedUpdate = new Date()
    console.log('Finished update rating in ' + (finishedUpdate.getTime() - finishedTime.getTime()).toString() + ' miliseconds.')
}

////////////// RECALCULATE BEGIN HERE

class ReCalculateRating {
    constructor(contestants) {
        this.contestants = contestants
    }

    getEloWinProbability(a, b) {
        let ra = a.rating, rb = b.rating
        return 1.0 / (1 + Math.pow(10, (rb - ra) / 400.0))
    }

    sortByRatingDesc() {
        return this.contestants.sort((a, b) => b.rating - a.rating)
    }

    getSeed(rating) {
        let extraContestant = {
            username: null,
            rank: 0,
            points: 0,
            rating: rating
        }

        let result = 1.0
        for (let other of this.contestants) {
            result += this.getEloWinProbability(other, extraContestant)
        }

        return result
    }

    getRatingToRank(rank) {
        let left = 1;
        let right = 8000;

        while (right - left > 1) {
            let mid = (left + right) / 2;

            if (this.getSeed(mid) < rank) {
                right = mid;
            } else {
                left = mid;
            }
        }

        return left;
    }

    ReCalculateRating() {
        for (let a of this.contestants) {
            a.seed = 1
            for (let b of this.contestants) {
                if (a == b) continue
                a.seed += this.getEloWinProbability(b, a)
            }
        }

        for (let contestant of this.contestants) {
            let midRank = Math.sqrt(contestant.rank * contestant.seed)
            contestant.needRating = this.getRatingToRank(midRank)
            contestant.delta = (contestant.needRating - contestant.rating) / 2
        }

        this.sortByRatingDesc()

        let sum = 0
        for (let c of this.contestants) {
            sum += c.delta
        }

        let inc = -sum / this.contestants.length - 1
        for (let contestant of this.contestants) {
            contestant.delta += inc
        }

        sum = 0
        let zeroSumCount = Math.min((4 * Math.round(Math.sqrt(this.contestants.length)), this.contestants.length));
        for (let i = 0; i < zeroSumCount; i++) {
            sum += this.contestants[i].delta;
        }

        inc = Math.min(Math.max(-sum / zeroSumCount, -10), 0);
        for (let contestant of this.contestants) {
            contestant.delta += inc;
        }
    }
}

load()
