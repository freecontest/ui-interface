'use strict';

const sequelize = require('../../db/default');
const Contest = require('../../db/contest');
const User = require('../../db/user');
const Registration = require('../../db/registration');

// [GET]   : get list contests ()
// [PUT]   : create a new contest (name, startTime, endTime, duration, writters)
// [DELETE]: delete a contest (id)
const availableMethod = ['POST'];

class AContest {
	constructor(pars, session, res) {
		this.pars = pars;
		this.session = session;
		this.out = res;
	};

	async POST() {
    let out = this.out;
    try {
      let cid = this.pars.cid || null;
      let uid = this.session.uid || null;
      
      if (cid == null || uid == null) {
        return this.out.json({
          err: 'Yêu cầu không được thực hiện vì máy chủ không thể nhận diện kỳ thi hoặc người dùng'
        })
      }

      let contestCond = {
        id: {
          $eq: cid
        }
      }
      let contestInfo = await Contest.findOne({where: contestCond});
      if (contestInfo === null) {
        return this.out.json({
          err: 'Kỳ thi không hợp lệ'
        })
      }

      let regisCond = {
        cid: cid, 
        uid: uid
      }
      let hasRegistered = await Registration.findOne({ where: regisCond });
      
      let currentTime = (new Date()).getTime()
      contestInfo = Object.assign(contestInfo.dataValues, {
        hasRegistered: (hasRegistered !== null),
        IsDuring: currentTime >= (new Date(contestInfo.startTime)).getTime()
      });
      return this.out.json(contestInfo)
    }
    catch(e) {
      console.log(e);
      return out.json({
        err: 'Đã có lỗi xảy ra khi đăng ký'
      })
    }
  }
};

module.exports = (req, res) => {
  console.log(req.method)
	if (availableMethod.indexOf(req.method) == -1) {
		res.json({ err: `we doesn't support this method.` });
		return ;
	} else {
		let pars = (req.method == 'GET')
				? req.query
				: (req.method == 'POST' || req.method == 'PUT' || req.method == 'PATCH')
					? req.body
					: {};

		let helper = new AContest(pars, req.session, res);
		helper[req.method]();
	};
};