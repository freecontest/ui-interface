'use strict';

const sequelize = require('../../db/default');
const Post = require('../../db/posts');
const Comment = require('../../db/comments')
const markdown = require('markdown').markdown;

const availableMethod = ['GET', 'POST', 'PUT', 'PATCH'];

class APost {
	constructor(pars, session, res) {
		this.pars = pars;
		this.session = session;
		this.out = res;
	};

	GET() {
		sequelize.authenticate()
		.then(() => {
			if (this.pars.postid !== null && this.pars.postid !== undefined) {
				Post.findAll({
					where: { id: this.pars.postid },
					order: [ [ 'createdAt', 'DESC' ]]
				}).then(async (posts) => {
					var res = [];
					for (var i = 0; i < posts.length; ++i) {
						let newObj = {
							id: posts[i].id,
							title: posts[i].title,
							content: posts[i].content,
							HTMLContent: markdown.toHTML(posts[i].content),
							author: posts[i].author,
							createdAt: posts[i].createdAt,
							updatedAt: posts[i].updatedAt
						};
						let cntComment = await Comment.count({ where: {
							pid: {
								$eq: posts[i].id
							}
						}})
						newObj.cntComment = cntComment
						res.push(newObj);
					};
					this.out.json(res);
				})
				.catch((err) => {
					this.out.json(err);
				});
			} else {
				Post.findAll({
					order: [ [ 'createdAt', 'DESC' ]]
				}).then(async (posts) => {
					var res = [];
					for (var i = 0; i < posts.length; ++i) {
						let newObj = {
							id: posts[i].id,
							title: posts[i].title,
							content: posts[i].content,
							HTMLContent: markdown.toHTML(posts[i].content),
							author: posts[i].author,
							createdAt: posts[i].createdAt,
							updatedAt: posts[i].updatedAt
						};
						let cntComment = await Comment.count({ where: {
							pid: {
								$eq: posts[i].id
							}
						}})
						newObj.cntComment = cntComment
						res.push(newObj);
					};
					this.out.json(res);
				})
				.catch((err) => {
					this.out.json(err);
				});
			};
		})
		.catch((err) => {
			console.log(err);
			this.out.json(err);
		});
	};

	POST() {
		sequelize.authenticate()
		.then(() => {
			if (!this.session.logined || this.session.role !== 'admin') {
				this.out.json({ err: `you must login first` });
			} else {
				let title = this.pars.title || null,
					content = this.pars.content || null;

				if (title == null || content == null) {
					this.out.json({ err: `you've forgot something` });
				} else {
					Post.create({
						title: title,
						content: content,
						author: this.session.username
					})
					.then((resu) => { this.out.json({ success: true }); })
					.catch((err) => { this.out.json({ err: err }); });
				};
			};
		})
		.catch((err) => {
			this.out.json({ err: err });
		});
	};

	PUT() {
		sequelize.authenticate()
		.then(() => {
			if (!this.session.logined || this.session.role !== 'admin') {
				this.out.json({ err: `you must login first` });
			} else {
				let title = this.pars.title || null,
					content = this.pars.content || null,
					pid = this.pars.pid || null;

				if (title == null || content == null || pid == null) {
					this.out.json({ err: `you've forgot something` });
				} else {
					Post.findAll({ where: { id: pid } })
					.then((posts) => {
						if (posts.length == 0) {
							this.out.json({ err: `cant find the post with id ${pid}` });
						} else {
							Post.update({
								title: title,
								content: content
							},
							{ where: { id: pid } })
							.then((resu) => { this.out.json({ success: true }); })
							.catch((err) => { this.out.json({ err: err }); });
						};
					})
					.catch((err) => {
						this.out.json({ err: err });
					});
				};
			};
		})
		.catch((err) => {
			this.out.json({ err: err });
		});
	};

	PATCH() {
		sequelize.authenticate()
		.then(() => {
			if (!this.session.logined || this.session.role !== 'admin') {
				this.out.json({ err: `you must login first` });
			} else {
				let pid = this.pars.pid || null;
				if (pid == null) {
					this.out.json({ err: `you've forgot something` });
				} else {
					Post.findAll({ where: { id: pid } })
					.then((posts) => {
						if (posts.length == 0) {
							this.out.json({ err: `cant find the post with id ${pid}` });
						} else {
							Post.destroy({ where: { id: pid } })
							.then((resu) => { this.out.json({ success: true }); })
							.catch((err) => { this.out.json({ err: err }); });
						};
					})
					.catch((err) => {
						this.out.json({ err: err });
					});
				};
			};
		})
		.catch((err) => {
			this.out.json({ err: err });
		});
	};
};

module.exports = (req, res) => {
	if (availableMethod.indexOf(req.method) == -1) {
		res.json({ err: `we doesn't support this method.` });
		return ;
	} else {
		let pars = (req.method == 'GET')
				? req.query
				: (req.method == 'POST' || req.method == 'PUT' || req.method == 'PATCH')
					? req.body
					: {};

		let helper = new APost(pars, req.session, res);
		helper[req.method]();
	};
};
