'use strict';

const sequelize = require('../../db/default');
const Post = require('../../db/posts');
const Like = require('../../db/likes');
const Reply = require('../../db/replies');
const Comment = require('../../db/comments')
const User = require('../../db/user')

// [POST]  : log into account (username, password)
const availableMethod = ['GET', 'POST', 'DELETE', 'PUT'];

class LikeHelper {
	constructor(uid, pid) {
		this.uid = uid;
		this.pid = pid;
	};

	like() {
		return new Promise((resolve) => {
			sequelize.authenticate()
			.then(() => {
				Post.findAll({ where: { id: this.pid } })
				.then((posts) => {
					if (posts.length == 0) {
						resolve({ err: `can't fint post with id ${this.pid}` });
					} else {
						Like.findAll({ where: { uid: this.uid, pid: this.pid } })
						.then((likes) => {
							if (likes.length !== 0) {
								resolve({ err: `you've liked this post` });
							} else {
								Like.create({
									uid: this.uid,
									pid: this.pid
								})
								.then((resu) => { resolve({ success: true }); })
								.catch((err) => { resolve({ err: err }); });
							};
						})
						.catch((err) => {
							resolve({ err: err });
						});
					};
				})
				.catch((err) => {
					resolve({ err: err });
				});
			})
			.catch((err) => {
				resolve({ err: err });
			});
		});
	};

	unlike() {
		return new Promise((resolve) => {
			sequelize.authenticate()
			.then(() => {
				Post.findAll({ where: { id: this.pid } })
				.then((posts) => {
					if (posts.length == 0) {
						resolve({ err: `can't fint post with id ${this.pid}` });
					} else {
						Like.findAll({ where: { uid: this.uid, pid: this.pid } })
						.then((likes) => {
							if (likes.length == 0) {
								resolve({ err: `you haven't like this post yet` })
							} else {
								Like.destroy({ where: { uid: this.uid, pid: this.pid } })
								.then((resu) => { resolve({ success: true }); })
								.catch((err) => { resolve({ err: err }); });
							};
						})
						.catch((err) => {
							resolve({ err: err });
						});
					};
				})
				.catch((err) => {
					resolve({ err: err });
				});
			})
			.catch((err) => {
				resolve({ err: err });
			});
		});
	};
};

class CommentHelper {
	constructor(uid, pid, cid, content) {
		this.uid = uid;
		this.pid = pid;
		this.cid = cid || null;
		this.content = content || null;
	};

	comment() {
		return new Promise((resolve) => {
			if (this.content == null || this.content == '') {
				resolve({ err: `you've forgot something` });
			} else {
				sequelize.authenticate()
				.then(() => {
					Post.findAll({ where: { id: this.pid } })
					.then((posts) => {
						if (posts.length == 0) {
							resolve({ err: `can't find post with id ${this.pid}` });
						} else {
							Comment.create({
								uid: this.uid,
								pid: this.pid,
								content: this.content
							})
							.then((resu) => { resolve({ success: true }); })
							.catch((err) => { resolve({ err: err }); });
						};
					})
					.catch((err) => {
						resolve({ err: err });
					});
				})
				.catch((err) => {
					resolve({ err: err });
				});
			};
		});
	};

	edit() {
		return new Promise((resolve) => {
			if (this.content == null) {
				resolve({ err: `you've forgot something` });
			} else {
				sequelize.authenticate()
				.then(() => {
					Comment.findAll({ where: { id: this.cid } })
					.then((comments) => {
						if (comments.length == 0) {
							resolve({ err: `cant' find comment with id ${this.cid}` });
						} else {
							comments = comments[0];
							if (comments.uid !== this.uid) {
								resolve({ err: `permission denied` });
							} else {
								Comment.update({
									content: this.content
								}, { where: { id: this.cid } })
								.then((resu) => { resolve({ success: true }); })
								.catch((err) => { resolve({ err: err }); });
							};
						};
					})
					.catch((err) => {
						resolve({ err: err });
					});
				})
				.catch((err) => {
					resolve({ err: err });
				});
			};
		});
	};

	delete() {
		return new Promise((resolve) => {
			sequelize.authenticate()
			.then(() => {
				Comment.findAll({ where: { id: this.cid } })
				.then((comments) => {
					if (comments.length == 0) {
						resolve({ err: `cant' find comment with id ${this.cid}` });
					} else {
						comments = comments[0];
						if (comments.uid !== this.uid) {
							resolve({ err: `permission denied` });
						} else {
							Comment.destroy({ where: { id: this.cid } })
							.then((resu) => { resolve({ success: true }); })
							.catch((err) => { resolve({ err: err }); });
						};
					};
				})
				.catch((err) => {
					resolve({ err: err });
				});
			})
			.catch((err) => {
				resolve({ err: err });
			});
		});
	};
};

class ReplyHelper {
	constructor(uid, pid, cid, rid, content) {
		this.uid = uid;
		this.pid = pid;
		this.cid = cid || null;
		this.rid = rid || null;
		this.content = content || null;
	};

	reply() {
		return new Promise((resolve) => {
			sequelize.authenticate()
			.then(() => {
				Post.findAll({ where: { id: this.pid } })
				.then((posts) => {
					if (posts.length == 0) {
						resolve({ err: `can't find post with id ${this.pid}` });
					} else {
						Comment.findAll({ where: { id: this.cid } })
						.then((comments) => {
							if (comments.length == 0) {
								resolve({ err: `can't find then comment with id ${this.cid}` });
							} else {
								if (this.content == null) {
									resolve({ err: `you've forgot something` });
								} else {
									Reply.create({
										uid: this.uid,
										cid: this.cid,
										content: this.content
									})
									.then((resu) => { resolve({ success: true }); })
									.catch((err) => { resolve({ err: err }) });
								};
							};
						})
						.catch((err) => { resolve({ err: err }); });
					};
				})
				.catch((err) => { resolve({ err: err }); });
			})
			.catch((err) => {
				resolve({ err: err });
			});
		});
	};

	edit() {
		return new Promise((resolve) => {
			if (this.content == null) {
				resolve({ err: `you've forgot something` });
			} else {
				sequelize.authenticate()
				.then(() => {
					Post.findAll({ where: { id: this.pid } })
					.then((posts) => {
						if (posts.length == 0) {
							resolve({ err: `can't find post with id ${this.pid}` })
						} else {
							Comment.findAll({ where: { id: this.cid } })
							.then((comments) => {
								if (comments.length == 0) {
									resolve({ err: `can't find comment with id ${this.cid}` })
								} else {
									Reply.findAll({ where: { id: this.rid } })
									.then((replies) => {
										if (replies.length == 0) {
											resolve({ err: `can't find reply with id ${this.rid}` });
										} else {
											replies = replies[0];
											if (replies.uid !== this.uid) {
												resolve({ err: `permission denied` });
											} else {
												Reply.update({
													content: this.content
												}, { where: { id: this.rid } })
												.then((resu) => { resolve({ success: true }); })
												.catch((err) => { resolve({ err: err }); });
											};
										};
									})
									.catch((err) => { resolve({ err: err }); });
								};
							})
							.catch((err) => { resolve({ err: err }); });
						};
					})
					.catch((err) => { resolve({ err: err }); });
				})
				.catch((err) => {
					resolve({ err: err });
				});
			};
		});
	};

	delete() {
		return new Promise((resolve) => {
			sequelize.authenticate()
			.then(() => {
				Reply.findAll({ where: { id: this.rid } })
				.then((replies) => {
					if (replies.length == 0) {
						resolve({ err: `can't find reply with id ${this.rid}` });
					} else {
						replies = replies[0];
						if (replies.uid !== this.uid) {
							resolve({ err: `permission denied` });
						} else {
							Reply.destroy({ where: { id: this.rid } })
							.then((resu) => { resolve({ success: true }); })
							.catch((err) => { resolve({ err: err }); });
						};
					};
				})
				.catch((err) => { resolve({ err: err }); });
			})
			.catch((err) => {
				resolve({ err: err });
			});
		});
	};
};

class Actions {
	constructor(pars, session, res) {
		this.pars = pars;
		this.session = session;
		this.out = res;
	};

	async GET() {
		if (this.pars.pid === null || this.pars.pid === undefined) {
			return this.out.json({ err: 'invalid' })
		}

		let method = this.pars.method;
		if (method !== 'comment') return this.out.json({ err: 'invalid' })

		let allComments = await Comment.findAll({
			where: {
				pid: {
					$eq: this.pars.pid
				}
			},
			order: [
				['createdAt', 'DESC']
			],
			limit: this.pars.limit,
			offset: this.pars.offset
		})

		let r = []
		for (let com of allComments) {
			com = com.dataValues
			let owner = await User.findOne({
				where: {
					id: {
						$eq: com.uid
					}
				}
			})
			com.owner = owner.dataValues || null
			r.push(com)
		}

		let cntComment = await Comment.count({
			where: {
				pid: {
					$eq: this.pars.pid
				}
			}
		})

		this.out.json({
			comments: r,
			cntComment: cntComment
		})
	}

	async POST() {
		if (!this.session.logined) {
			this.out.json({ err: `you must login` });
			return ;
		} else {
			let method = this.pars.method;
			if (method == 'like') {
				let uid = this.pars.uid,
					pid = this.pars.pid;

				let lHelper = new LikeHelper(uid, pid);
				this.out.json(lHelper.like());
			} else
			if (method == 'comment') {
				let uid = this.session.uid,
					pid = this.pars.pid,
					cid = this.pars.cid || null,
					content = this.pars.content || null;
				let cHelper = new CommentHelper(uid, pid, cid, content);
				let r = await cHelper.comment()
				this.out.json(r);
			} else
			if (method == 'reply') {
				let uid = this.pars.uid,
					pid = this.pars.pid,
					cid = this.pars.cid || null,
					rid = this.pars.rid || null,
					content = this.pars.content || null;

				let rHelper = new ReplyHelper(uid, pid, cid, rid, content);
				this.out.json(rHelper.reply());
			} else
			if (method == 'delete_comment') {
				let uid = this.session.uid || null,
					pid = this.pars.pid || null,
					cid = this.pars.cid || null,
					content = this.pars.content || null;

				let cHelper = new CommentHelper(uid, pid, cid, content);
				let r = await cHelper.delete()
				this.out.json(r);
			} else {
				this.out.json({ err: `we doesn't support this method.` });
			};
		};
	};

	async PUT() {
		if (!this.session.logined) {
			this.out.json({ err: `you must login` });
			return ;
		} else {
			let method = this.pars.method;
			if (method == 'like') {
				let uid = this.pars.uid,
					pid = this.pars.pid;

				lHelper = new LikeHelper(uid, pid);
				this.out.json(lHelper.unlike());
			} else
			if (method == 'comment') {
				let uid = this.session.uid,
					pid = this.pars.pid || null,
					cid = this.pars.cid || null,
					content = this.pars.content || null;

				let cHelper = new CommentHelper(uid, pid, cid, content);
				let r = await cHelper.edit()
				this.out.json(r);
			} else
			if (method == 'reply') {
				let uid = this.pars.uid,
					pid = this.pars.pid,
					cid = this.pars.cid || null,
					rid = this.pars.rid || null,
					content = this.pars.content || null;

				rHelper = new ReplyHelper(uid, pid, cid, rid, content);
				this.out.json(rHelper.edit());
			} else {
				this.out.json({ err: `we doesn't support this method.` });
			};
		};
	};

	async DELETE() {
		if (!this.session.logined) {
			this.out.json({ err: `you must login` });
			return ;
		} else {
			let method = this.pars.method;
			if (method == 'like') {
				let uid = this.pars.uid,
					pid = this.pars.pid;

				lHelper = new LikeHelper(uid, pid);
				this.out.json(lHelper.unlike());
			} else
			if (method == 'comment') {
				let uid = this.session.uid || null,
					pid = this.pars.pid || null,
					cid = this.pars.cid || null,
					content = this.pars.content || null;

				let cHelper = new CommentHelper(uid, pid, cid, content);
				let r = await cHelper.delete()
				this.out.json(r);
			} else
			if (method == 'reply') {
				let uid = this.pars.uid,
					pid = this.pars.pid,
					cid = this.pars.cid || null,
					rid = this.pars.rid || null,
					content = this.pars.content || null;

				rHelper = new ReplyHelper(uid, pid, cid, rid, content);
				this.out.json(rHelper.delete());
			} else {
				this.out.json({ err: `we doesn't support this method.` });
			};
		};
	};
};

module.exports = (req, res) => {
	if (availableMethod.indexOf(req.method) == -1) {
		res.json({ err: `we doesn't support this method` });
		return ;
	} else {
		let pars = (req.method == 'GET' || req.method == 'DELETE')
				? req.query
				: (req.method == 'POST' || req.method == 'PUT')
					? req.body
					: {};

		let helper = new Actions(pars, req.session, res);
		helper[req.method]();
	};
};
