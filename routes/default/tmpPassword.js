'use strict';

const tmpPassword = require('../../db/tmpPassword');

const availableMethod = ['POST'];

class ATmpPassWord {
	constructor(pars, session, res) {
		this.pars = pars;
		this.session = session;
		this.out = res;
	};

	async POST()
	{
		if (!this.session.logined) 
		{
			return { err: `you must login first.` }	
		}

		let uid = this.session.uid || null;
		let cid = this.pars.cid || null;

		if (cid === null || cid === null) 
		{
			return { err: `contest_id(cid) is required` }
		}

		let cond = {
			uid: {
				$eq: uid
			},
			cid: {
				$eq: cid
			}
		}

		let randPass = await tmpPassword.findOne({ where: cond })

		if (randPass === null) {
			return { 
				err: `can't find tmp password for uid ${uid} in contest ${cid}`
			} 
		}

		return {
			success: true,
			tmpPassword: randPass.dataValues.tmpPassword
		}
	}
};

module.exports = (req, res) => 
{
	if (availableMethod.indexOf(req.method) == -1) 
	{
		return res.json({ err: `we doesn't support this method.` })
	} 
	else 
	{
		let pars = (req.method == 'GET') 
				? req.query
				: (req.method == 'POST' || req.method == 'PUT') 
					? req.body 
					: {}

		let helper = new ATmpPassWord(pars, req.session, res)
		helper[req.method]().then((response) => res.json(response))
	};
};