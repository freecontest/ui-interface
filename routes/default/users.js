'use strict';

const sequelize = require('../../db/default');
const User = require('../../db/user');
const Registration = require('../../db/registration');
const logsUpdateRating = require('../../db/logsUpdateRating')
const Contest = require('../../db/contest')
const md5 = require('md5');

const bcrypt = require('bcrypt')

// [GET]    : get all users
// [POST]	: update user's avatar
// [PUT]	: update user's information
// [DELETE]	: delete user
const availableMethod = ['GET', 'POST', 'PUT', 'DELETE'];

class userManager {
	constructor(pars, session, res) {
		this.pars = pars;
		this.session = session;
		this.out = res;
	};

	GET() {
		sequelize.authenticate()
		.then(() => {
			let attr = ['id', 'username', 'name', 'schoolName', 'yob',
						'color', 'rating', 'avatar', 'role', 'createdAt', 'updatedAt', 'showOnRanking'];

			let username = this.pars.username || null;
			let limit = this.pars.limit || null;
			let offset = this.pars.offset || null

			if (username == null) {
				User.findAll({
					attributes: attr,
					order: [ [ 'rating', 'DESC' ], [ 'createdAt', 'ASC' ] ],
					limit: (limit == null ? 99999 : limit),
					offset: (offset == null ? 0 : offset),
					where: {
						showOnRanking: {
							$eq: true 
						},
						rating: {
							$gt: 0
						}
					}
				}).then((users) => {
					this.out.json(users);
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			} else {
				User.findAll({
					attributes: attr,
					where: {
						username: username
					}
				})
				.then(async (users) => {
					if (users.length == 0) {
						return this.out.json({ err: `can't find user with username ${username}` });
					} else {
						users = users[0];
						var user = {};

						// trash lines
						user.id = users.id;
						user.username = users.username;
						user.name = users.name;
						user.schoolName = users.schoolName;
						user.yob = users.yob;
						user.createdAt = users.createdAt;
						user.updatedAt = users.updatedAt;
						user.color = users.color;
						user.rating = users.rating;
						user.avatar = users.avatar;
						user.role = users.role;

						Registration.findAll({
							where: {
								uid: users.id
							}
						})
						.then(async (regis) => {
							user.contests = regis;
							let logsUpdateRat = await logsUpdateRating.findAll({ where: { username: { $eq: username } } , order: [[ 'createdAt', 'DESC' ]]})
							for (let index in logsUpdateRat) {
								logsUpdateRat[index] = logsUpdateRat[index].dataValues
								let time = await Contest.findOne({ where: { id: { $eq: logsUpdateRat[index].contestID } } }).then(d => d.dataValues)
								logsUpdateRat[index].startTime = time.startTime
							}
							user.logsUpdateRating = logsUpdateRat
							this.out.json(user);
						})
						.catch((err) => {
							this.out.json({ err: err });
						});
					};
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		})
		.catch((err) => {
			this.out.json({ err: err });
		});
	};

	POST() {
		if (!this.session.logined) {
			this.out.json({ err: `you must login first.` });
			return ;
		} else {
			let uid = this.session.uid || null,
				email = this.pars.email || null;

			let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (uid == null || email == null || re.test(email) == false) {
				this.out.json({
					err: `you've forgot something.`
				});
			} else {
				sequelize.authenticate()
				.then(() => {
					User.findAll({ where: { id: uid } }).then((user) => {
						if (user.length == 0) {
							this.out.json({
								err: `can't find user with id ${uid}`
							});
							return ;
						} else {
							User.update({
								avatar: md5(email.toLowerCase())
							}, { where: { id: uid } })
							.then((resu) => {
								User.findAll({ where: { id: uid } }).then((user) => {
									user = user[0];
									this.session.logined = true;
									this.session.uid = user.id;
									this.session.username = user.username;
									this.session.name = user.name;
									this.session.schoolName = user.schoolName;
									this.session.yob = user.yob;
									this.session.color = user.color;
									this.session.rating = user.rating;
									this.session.role = user.role;
									this.session.avatar = user.avatar;

									var sess = Object.assign({success: true}, this.session);
									delete sess.cookie;
									this.out.json(sess);
								})
								.catch((err) => {
									this.out.json({ err: err });
								});
							})
							.catch((err) => { this.out.json({ err: err }); });
						};
					})
					.catch((err) => {
						this.out.json({ err: err });
					})
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		};
	}

	PUT() {
		if (!this.session.logined) {
			this.out.json({ err: `you must login first.` });
			return ;
		} else {
			let uid = this.session.uid || null,
				password = this.pars.password || null,
				name = this.pars.name || null,
				schoolName = this.pars.schoolName || null,
				yob = this.pars.yob || null,
				showOnRanking = this.pars.showOnRanking;

			if (uid == null || name == null || schoolName == null || yob == null
			|| name.length < 1 || schoolName.length < 1 || yob.toString().length < 4) {
				this.out.json({
					err: `you've forgot something.`
				});
				return ;
			} else {
				sequelize.authenticate()
				.then(() => {
					User.findAll({ where: { id: uid } }).then(async (user) => {
						if (user.length == 0) {
							this.out.json({
								err: `can't find user with id ${uid}`
							});
							return ;
						} else {
							if (password !== null && password.length > 0) {
								password = await bcrypt.hash(password, 10)

								User.update({
									password: password,
									name: name,
									schoolName: schoolName,
									yob: yob,
									showOnRanking: showOnRanking
								}, { where: { id: uid } })
								.then((resu) => {
									User.findAll({ where: { id: uid } }).then((user) => {
										user = user[0];
										this.session.logined = true;
										this.session.uid = user.id;
										this.session.username = user.username;
										this.session.name = user.name;
										this.session.schoolName = user.schoolName;
										this.session.yob = user.yob;
										this.session.color = user.color;
										this.session.rating = user.rating;
										this.session.role = user.role;
										this.session.avatar = user.avatar;
										this.session.showOnRanking = user.showOnRanking;

										var sess = Object.assign({success: true}, this.session);
										delete sess.cookie;
										this.out.json(sess);
									})
									.catch((err) => {
										this.out.json({ err: err });
									});
								})
								.catch((err) => {
									this.out.json({ err: err });
								});
							} else {
								User.update({
									name: name,
									schoolName: schoolName,
									yob: yob,
									showOnRanking: showOnRanking
								}, { where: { id: uid } })
								.then((resu) => {
									User.findAll({ where: { id: uid } }).then((user) => {
										user = user[0];
										this.session.logined = true;
										this.session.uid = user.id;
										this.session.username = user.username;
										this.session.name = user.name;
										this.session.schoolName = user.schoolName;
										this.session.yob = user.yob;
										this.session.color = user.color;
										this.session.rating = user.rating;
										this.session.role = user.role;
										this.session.avatar = user.avatar;
										this.session.showOnRanking = user.showOnRanking;

										var sess = Object.assign({success: true}, this.session);
										delete sess.cookie;
										this.out.json(sess);
									})
									.catch((err) => {
										this.out.json({ err: err });
									});
								})
								.catch((err) => {
									this.out.json({ err: err });
								});
							};
						};
					})
					.catch((err) => {
						this.out.json({ err: err });
					});
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		};
	};

	DELETE() {
		if (!this.session.logined) {
			this.out.json({ err: `you must login first.` });
			return ;
		} else {
			let uid = this.session.uid || null;
			if (uid == null) {
				this.out.json({ err: `you've forgot something.` });
				return ;
			} else {
				sequelize.authenticate()
				.then(() => {
					User.findAll({ where: { id: uid } }).then((user) => {
						if (user.length == 0) {
							this.out.json({ err: `can't find user with id ${uid}` });
							return ;
						} else {
							User.destroy({ where: { id: uid } })
							.then((resu) => {
								this.session.destroy(); // delete session
								this.out.json({ success: true });
							})
							.catch((err) => {
								this.out.json({ err: err });
							});
						};
					})
					.catch((err) => {
						this.out.json({ err: err });
					});
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		};
	};
};

module.exports = (req, res) => {
	if (availableMethod.indexOf(req.method) == -1) {
		res.json({ err: `we doesn't support this method.` });
		return ;
	} else {
		let pars = (req.method == 'GET')
				? req.query
				: (req.method == 'POST' || req.method == 'PUT')
					? req.body
					: {};

		let helper = new userManager(pars, req.session, res);
		helper[req.method]();
	};
};
