'use strict';

// [GET] : show me the index
const availableMethod = ['GET'];

class Index {
	constructor(pars, session, res) {
		this.pars = pars;
		this.session = session;
		this.out = res;
	};

	GET() {
		this.out.render('index.html', {
			author: 'rknguyen',
			staticDir: process.env.staticDir,
			angularCDN: process.env.angularCDN,
			brandName: process.env.brandName
		});
	};
};

module.exports = (req, res) => {
	if (availableMethod.indexOf(req.method) == -1) {
		res.json({ err: `we doesn't support this method.` });
		return ;
	} else {
		let pars = (req.method == 'GET') 
				? req.query
				: (req.method == 'POST' || req.method == 'PUT') 
					? req.body 
					: {};

		let helper = new Index(pars, req.session, res);
		helper[req.method]();
	};
};