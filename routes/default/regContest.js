'use strict';

const Contest = require('../../db/contest');
const Registration = require('../../db/registration');
const generator = require('generate-password');
const tmpPassword = require('../../db/tmpPassword');
const CMSRegister = require('../../lib/registerContest')

const availableMethod = ['POST'];

class AContest {
  constructor(pars, session, res) {
    this.pars = pars;
    this.session = session;
    this.out = res;
  };

  async POST() {
    let out = this.out;
    try {
      let cid = this.pars.cid || null;
      let uid = this.session.uid || null;
      let displayOnRanking = this.pars.displayOnRanking || false;
      
      if (cid == null || uid == null) {
        return this.out.json({
          err: 'Yêu cầu không được thực hiện vì máy chủ không thể nhận diện kỳ thi hoặc người dùng'
        })
      }

      let contestCond = {
        id: {
          $eq: cid
        }
      }
      let contestInfo = await Contest.findOne({where: contestCond});
      if (contestInfo === null) {
        return this.out.json({
          err: 'Kỳ thi không hợp lệ'
        })
      }

      contestInfo = contestInfo.dataValues

      let regisCond = {
        cid: cid, 
        uid: uid
      }
      let hasRegistered = await Registration.findOne({ where: regisCond });
      
      if (hasRegistered !== null) {
        return this.out.json({
          err: 'Bạn đã đăng ký kỳ thi này'
        })
      }

      let currentTime = (new Date()).getTime()
      if (currentTime >= (new Date(contestInfo.startTime)).getTime()) {
        displayOnRanking = false;
      }

      if (contestInfo.code == 1) {
        displayOnRanking = false;
      }

      let randomPassword = generator.generate({
        length: 30,
        numbers: true
      })
      
      let registerCallback = await CMSRegister(
        this.session.username,
        randomPassword,
        this.session.name,
        this.session.schoolName,
        contestInfo.id_contest,
        displayOnRanking
      )

      if (registerCallback.success) {
        let newState = {
          uid: uid,
          cid: cid,
          tmpPassword: randomPassword
        }
        await tmpPassword.create(newState)
  
        let newRegistration = {
          uid: uid,
          cid: cid
        }
        await Registration.create(newRegistration)

        return this.out.json({ success: true })
      } else {
        return this.out.json({
          err: registerCallback.err
        })
      }
    }
    catch(e) {
      console.log(e);
      return out.json({
        err: 'Đã có lỗi xảy ra khi đăng ký'
      })
    }
  }
};

module.exports = (req, res) => {
  if (availableMethod.indexOf(req.method) == -1) {
    res.json({ err: `we doesn't support this method.` });
    return ;
  } else {
    let pars = (req.method == 'GET')
        ? req.query
        : (req.method == 'POST' || req.method == 'PUT' || req.method == 'PATCH')
          ? req.body
          : {};

    let helper = new AContest(pars, req.session, res);
    helper[req.method]();
  };
};
