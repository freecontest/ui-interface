'use strict';

const User = require('../../db/user');
const ExecFile = require('child_process').execFile;
const Contest = require('../../db/contest');
const sequelize = require('../../db/default');
const generator = require('generate-password');
const tmpPassword = require('../../db/tmpPassword');
const Registration = require('../../db/registration');

// cid: contest id
// uid: user id
// rid: registered id

// [GET]   : get all registration
// [PUT]   : register a contest (cid, uid in session)
// [DELETE]: delete a registration (rid)
const availableMethod = ['GET', 'PUT', 'DELETE', 'POST'];

class ARegistration {
	constructor(pars, session, res) {
		this.pars = pars;
		this.session = session;
		this.out = res;
	};

	GET() {
		sequelize.authenticate()
		.then(() => {
			Registration.findAll().then((regs) => {
				this.out.json(regs);
			})
			.catch((err) => {
				this.out.json({ err: err });
			});
		})
		.catch((err) => {
			this.out.json({ err: err });
		});
	};

	POST() {
		let out = this.out;
		let pars = this.pars;
		(async function() {
			await sequelize.authenticate();
			let cid = pars.cid || null;
			if (cid == null) {
				out.json({ err: `you've forgot something` });
				return ;
			};
			
			let regis = [];
			await Registration.findAll({
					where: { cid: cid },
					order: [ [ 'createdAt', 'DESC' ]]
				  })
				  .then((r) => {
				  	if (r.length == 0) { out.json({err: `.... err roi :'(`}); }
				  	else {
				  		for (var i = 0; i < r.length; ++i) {
				  			regis.push({
				  				uid: r[i].uid,
				  				createdAt: r[i].createdAt
				  			});
				  		};
				  	};
				  })
				  .catch((e) => { out.json(e); });

			let res = [];
			for (var i = 0; i < regis.length; ++i) {
				let uid = regis[i].uid;
				await User.findAll({ where: { id: uid } }).then((user) => {
					if (user.length > 0) {
						user = user[0];
						res.push({
							username: user.username,
							color: user.color,
							createdAt: regis[i].createdAt
						});
					};
				})
				.catch((e) => { out.json({ err: e }); });
			};

			out.json(res);
		})();
	};

	PUT() {
		if (!this.session.logined) {
			this.out.json({ err: `you must login first.` });
			return ;
		} else {
			let cid = this.pars.cid || null,
				uid = this.session.uid || null;

			if (cid == null || uid == null) {
				this.out.json({ err: `you've forgot something.` });
				return ;
			} else {
				sequelize.authenticate()
				.then(() => {
					Contest.findAll({ where: { id: cid } }).then((contests) => {
						if (contests.length == 0) {
							this.out.json({
								err: `can't find the contest with id ${cid}`
							});
						} else {
							let contest = contests[0];
							Registration.create({
								uid: uid,
								cid: cid
							})
							.then((resu) => {
								// create random password
								let randomPassword = generator.generate({
									length: 30,
									numbers: true
								})

								tmpPassword.create({
									uid: uid,
									cid: cid,
									tmpPassword: randomPassword
								})
								.then((resss) => {
									User.findAll({ where: { id: uid } }).then((users) => {
										if (users.length == 0) {
											return
											this.out.json({ err: `can't find user with id ${uid}` });
										} else {
											let out = this.out;
											let user = users[0];
											let username = user.username;
											let password = randomPassword;
											let schoolName = user.schoolName;
											let id_contest = contest.id_contest;
											let name = user.name;
											let hidden = contest.code;

											let args = ["-p", password, name, schoolName, username];
											ExecFile("cmsAddUser", args, (err, stdout, stderr) => {
												let args2 = ["-c", id_contest, username]
												if (hidden == 1) {
													args2.push("--hidden");
												}
												ExecFile("cmsAddParticipation", args2,
												function (err2, stdout2, stderr2) {
													return out.json({ success: true });
												});
											});
										}
									})
									.catch((err) => { this.out.json({ err: err }); });
								})
								.catch((err) => { this.out.json({ err: err }); });
							})
							.catch((err) => { this.out.json({ err: err }); });
						};
					})
					.catch((err) => {
						this.out.json({ err: err });
					});
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		};
	};

	DELETE() {
		if (!this.session.logined) {
			this.out.json({
				err: `you must login first`
			});
		} else {
			let rid = this.pars.rid || null;
			if (rid == null) {
				this.out.json({
					err: `you've forgot something`
				});
			} else {
				sequelize.authenticate()
				.then(() => {
					Registration.findAll({ where: { id: rid } }).then((regs) => {
						if (regs.length == 0) {
							this.out.json({
								err: `can't find the registered with id ${rid}`
							});
						} else {
							Registration.destroy({ where: { id: rid } })
							.then((resu) => { this.out.json({ success: true }); })
							.catch((err) => { this.out.json({ err: err }); });
						};
					})
					.catch((err) => {
						this.out.json({ err: err });
					});
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		};
	};
};

module.exports = (req, res) => {
	if (availableMethod.indexOf(req.method) == -1) {
		res.json({ err: `we doesn't support this method.` });
		return ;
	} else {
		let pars = (req.method == 'GET') 
				? req.query
				: (req.method == 'POST' || req.method == 'PUT') 
					? req.body 
					: {};

		let helper = new ARegistration(pars, req.session, res);
		helper[req.method]();
	};
};

