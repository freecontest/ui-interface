'use strict';

const sequelize = require('../../db/default');
const User = require('../../db/user');

const bcrypt = require('bcrypt')

// [GET]   : get the user's session
// [POST]  : log into account (username, password)
// [PUT]   : create a new account (username, password, name, schoolName, you)
// [DELETE]: logout
const availableMethod = ['GET', 'POST', 'PUT', 'DELETE'];

class Auth {
	constructor(pars, session, res) {
		this.pars = pars;
		this.session = session;
		this.out = res;
	};

	async GET() {
		var sess = Object.assign({}, this.session);
		delete sess.cookie;
		if (!sess.logined) {
			return this.out.json({ logined: false })
		}
		let cond = { id: { $eq: sess.uid } }
		let user = await User.findOne({ where: cond })
		this.out.json({
			logined: true,
			...user.dataValues
		})
	};

	POST() {
		if (this.session.logined) {
			this.out.json({ err: `Bạn đã đăng nhập rồi.` });
			return ;
		};

		let username = this.pars.username || null,
			password = this.pars.password || null;

		if (username == null || password == null) {
			this.out.json({
				err: `Bạn đã quên một số trường bắt buộc.`	
			});
			return ;
		};

		sequelize.authenticate()
		.then(() => {
			User.findAll({ where: { username: username } }).then(async (user) => {
                const password_match = await bcrypt.compare(password, user[0].password)
				if (user.length == 0 || !password_match) {
					this.out.json({
						err: `Đăng nhập không thành công.`
					});
				} else {
					this.session.logined = true;
					this.session.uid = user[0].id;
					this.session.username = user[0].username;
					this.session.name = user[0].name;
					this.session.schoolName = user[0].schoolName;
					this.session.yob = user[0].yob;
					this.session.color = user[0].color;
					this.session.rating = user[0].rating;
					this.session.role = user[0].role;
					this.session.avatar = user[0].avatar;
					this.session.showOnRanking = user[0].showOnRanking;

					var sess = Object.assign({}, this.session);
					delete sess.cookie;
					this.out.json(sess);
				};
			})
			.catch((err) => {
				this.out.json({ err: err });
			});
		})
		.catch((err) => {
			this.out.json({ err: err });
		});
	};

	PUT() {
		if (this.session.logined) {
			this.out.json({ err: `please logout first.` });
			return ;
		};

		let username = this.pars.username || null,
			password = this.pars.password || null,
			name = this.pars.name || null,
			schoolName = this.pars.schoolName || null,
			yob = this.pars.yob || null;

		if (/^([A-Za-z0-9-_])*$/.test(username) === false) {
			return this.out.json({
				err: 'Tên đăng nhập chỉ có thể là A-Z, a-z, 0-9 và _ (dấu gạch ngang dưới)'
			})
		}
			
		if (username == null || password == null || name == null
		|| schoolName == null || yob == null || username.length < 1 
		|| password.length < 1 || name.length < 1 || schoolName.length < 1
		|| yob.toString().length < 4) {
			this.out.json({
				err: 'Thông tin bị thiếu'
			});
			return ;
		};

		sequelize.authenticate()
		.then(() => {
			User.findAll({ where: { username: username } }).then(async (user) => {
				if (user.length) {
					this.out.json({
						err: 'Tên đăng nhập đã tồn tại'
					});
				} else {
					password = await bcrypt.hash(password, 10);

					User.create({
						username: username,
						password: password,
						name: name,
						schoolName: schoolName,
						yob: yob
					})
					.then((resu) => { this.out.json({ success: true }); })
					.catch((err) => { this.out.json({ err: err }); });
				};
			})
			.catch((err) => {
				this.out.json({ err: err });
			});	
		})
		.catch((err) => {
			this.out.json({ err: err });
		});
	};

	DELETE() {
		if (!this.session.logined) {
			this.out.json({ err: `Hãy đăng nhập trước.` });
			return ;
		};

		this.session.destroy();
		this.out.json({ success: true });
	};
};

module.exports = async (req, res) => {
	if (availableMethod.indexOf(req.method) == -1) {
		res.json({ err: `we doesn't support this method.` });
		return ;
	} else {
		let pars = (req.method == 'GET') 
				? req.query
				: (req.method == 'POST' || req.method == 'PUT') 
					? req.body 
					: {};

		let helper = new Auth(pars, req.session, res);
		await helper[req.method]();
	};
};
