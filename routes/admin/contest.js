'use strict';

const sequelize = require('../../db/default');
const Contest = require('../../db/contest');
const User = require('../../db/user');
const Registration = require('../../db/registration');

// [GET]   : get list contests ()
// [PUT]   : create a new contest (name, startTime, endTime, duration, writters)
// [DELETE]: delete a contest (id)
const availableMethod = ['GET', 'PUT', 'PATCH', 'POST'];

class AContest {
	constructor(pars, session, res) {
		this.pars = pars;
		this.session = session;
		this.out = res;
	};

	GET() {
		sequelize.authenticate()
		.then(() => {
			let cid = this.pars.cid || null;
			if (cid == null) {
				Contest.findAll({ order: [ [ 'startTime', 'ASC' ]] }).then((contests) => {
					let contest = []
					for (var i = 0; i < contests.length; ++i) {
						contest.push(contests[i].dataValues)
					}

					let out = this.out;
					let session = this.session;
					let pars = this.pars;
					(async function() {
						for (var i = 0; i < contest.length; ++i) {
							let contestid = contest[i].id;
							await Registration.findAll({
								where: {
									uid: (pars.uid ? pars.uid : (session.logined ? session.uid : 0)),
									cid: contestid
								}
							}).then((res) => {
								contest[i].isRegistered = (res.length > 0);
							})
							.catch((err) => {
								out.json({ err: err });
							});

							contest[i].hasRegistered = false;
							if (session.logined) {
								let regisCond = {
									cid: contest[i].id, 
									uid: session.uid
								}
								let hasRegistered = await Registration.findOne({ where: regisCond });
								contest[i].hasRegistered = (hasRegistered !== null);
							}

							await Registration.count({
								where: { cid: contestid }
							}).then((c) => {
								contest[i].cntReg = c;
							})
							.catch((err) => {
								out.json({ err: err });
							});

							let wrs = contest[i].writters;
							contest[i].wrrts = [];
							for (var j = 0; j < wrs.length; ++j) {
								let username = wrs[j];
								await User.findAll({ where: { username: username } }).then((res) => {
									contest[i].wrrts.push(
										(res.length == 0)
										? {
											username: username,
											color: 1
										}
										: {
											username: res[0].username,
											color: res[0].color
										}
									);
								})
								.catch((err) => {
									out.json({ err: err });
								});
							};
						};
						out.json(contest);
					})();
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			} else {
				Contest.findAll({ where: { id: cid } }).then((contests) => {
					this.out.json(contests);
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		})
		.catch((err) => {
			this.out.json({ err: err });
		});
	};

	PUT() {
		if (!this.session.logined || this.session.role !== 'admin') {
			this.out.json({ err: `you must login first.` });
			return ;
		} else {
			let name = this.pars.name || null,
				startTime = this.pars.startTime || null,
				endTime = this.pars.endTime || null,
				duration = this.pars.duration || null,
				writters = this.pars.writters || [];

			if (name == null || startTime == null || endTime == null || duration == null
				|| writters == []) {
				this.out.json({ err: `you've forgot something.` });
				return ;
			} else {
				sequelize.authenticate()
				.then(() => {
					Contest.create({
						name: name,
						startTime: startTime,
						endTime: endTime,
						duration: duration,
						writters: writters
					})
					.then((resu) => { this.out.json({ success: true }); })
					.catch((err) => { this.out.json({ err: err }); });
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		};
	};

	PATCH() {
		if (!this.session.logined || this.session.role !== 'admin') {
			this.out.json({ err: `you must login first.` });
			return ;
		} else {
			let name = this.pars.name || null,
				startTime = this.pars.startTime || null,
				endTime = this.pars.endTime || null,
				duration = this.pars.duration || null,
				writters = this.pars.writters || [],
				cid = this.pars.cid || null,
				id_contest = this.pars.id_contest || null,
				code = this.pars.code,
				practice_url = this.pars.practice_url,
				facebook_url = this.pars.facebook_url;
			if (name == null || startTime == null || endTime == null || duration == null
				|| writters == [] || cid == null) {
				this.out.json({ err: `you've forgot something.` });
				return ;
			} else {
				sequelize.authenticate()
				.then(() => {
					Contest.findAll({
						where: { id: cid }
					})
					.then((contests) => {
						if (contests.length == 0) {
							this.out.json({ err: `can't find contest with id ${cid}` });
						} else {
							let newState = {
								name: name,
								startTime: startTime,
								endTime: endTime,
								duration: duration,
								writters: writters,
								id_contest: id_contest,
								code: code
							}
							if (practice_url != null && practice_url != undefined) {
								newState = Object.assign(newState, {
									practice_url: practice_url
								});
							}
							if (facebook_url != null && facebook_url != undefined) {
								newState = Object.assign(newState, {
									facebook_url: facebook_url
								});
							}
							Contest.update(newState, {
								where: { id: cid }
							})
							.then((resu) => { this.out.json({ success: true }); })
							.catch((err) => { console.log(err); this.out.json({ err: err }); });
						};
					})
					.catch((err) => {
						console.log(err);
						this.out.json({ err: err });
					});
				})
				.catch((err) => {
					console.log(err);
					this.out.json({ err: err });
				});
			};
		};
	};

	POST() {
		if (!this.session.logined || this.session.role !== 'admin') {
			this.out.json({ err: `you must login first.` });
			return ;
		} else {
			let cid = this.pars.cid || null;
			if (cid == null) {
				this.out.json({ err: `you must fill the cid.` });
				return ;
			} else {
				sequelize.authenticate()
				.then(() => {
					Contest.findAll({ where: {id : cid } }).then((contest) => {
						if (contest.length == 0) {
							this.out.json({
								err: `can't find the contest with id ${cid}`
							});
						} else {
							Contest.destroy({ where: { id: cid } })
							.then((resu) => { this.out.json({ success: true }) })
							.catch((err) => { this.out.json({ err: err }); });
						};
					}).
					catch((err) => {
						this.out.json({ err: err });
					});
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		};
	};
};

module.exports = (req, res) => {
	if (availableMethod.indexOf(req.method) == -1) {
		res.json({ err: `we doesn't support this method.` });
		return ;
	} else {
		let pars = (req.method == 'GET')
				? req.query
				: (req.method == 'POST' || req.method == 'PUT' || req.method == 'PATCH')
					? req.body
					: {};

		let helper = new AContest(pars, req.session, res);
		helper[req.method]();
	};
};
