'use strict';

const sequelize = require('../../db/default');
const User = require('../../db/user');

// [GET]    : get all users
// [PUT]	: update user's information
// [DELETE]	: delete user
const availableMethod = ['GET', 'PUT', 'DELETE'];

class userManager {
	constructor(pars, session, res) {
		this.pars = pars;
		this.session = session;
		this.out = res;
	};

	GET() {
		if (!this.session.logined || this.session.role !== 'admin') {
			this.out.json({ err: `you must login first.` });
			return ;
		} else {
			sequelize.authenticate()
			.then(() => {
				let uid = this.pars.uid || null;
				if (uid !== null) {
					User.findAll({
						where: { id: uid }
					}).then((users) => {
						this.out.json(users);
					})
					.catch((err) => {
						this.out.json({ err: err });
					});
				} else {
					User.findAll().then((users) => {
						this.out.json(users);
					})
					.catch((err) => {
						this.out.json({ err: err });
					});
				};
			})
			.catch((err) => {
				this.out.json({ err: err });
			});
		};
	};

	PUT() {
		if (!this.session.logined || this.session.role !== 'admin') {
			this.out.json({ err: `you must login first.` });
			return ;
		} else {
			let uid = this.pars.uid || null,
				username = this.pars.username || null,
				password = this.pars.password || null,
				color = this.pars.color,
				role = this.pars.role || null,
				rating = this.pars.rating;

			if (uid == null || username == null || password == null || role == null) {
				this.out.json({
					err: `you've forgot something.`
				});
				return ;
			} else {
				sequelize.authenticate()
				.then(() => {
					User.findAll({ where: { id: uid } }).then((user) => {
						if (user.length == 0) {
							this.out.json({
								err: `can't find user with id ${uid}`
							});
							return ;
						} else {
							User.update({
								username: username,
								password: password,
								color: color,
								role: role,
								rating: rating
							}, { where: { id: uid } })
							.then((resu) => { this.out.json({ success: true }); })
							.catch((err) => { this.out.json({ err: err }); });
						};
					})
					.catch((err) => {
						this.out.json({ err: err });
					});	
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		};
	};

	DELETE() {
		if (!this.session.logined || this.session.role !== 'admin') {
			this.out.json({ err: `you must login first.` });
			return ;
		} else {
			let uid = this.pars.uid || null;
			if (uid == null) {
				this.out.json({ err: `you've forgot something.` });
				return ;
			} else {
				sequelize.authenticate()
				.then(() => {
					User.findAll({ where: { id: uid } }).then((user) => {
						if (user.length == 0) {
							this.out.json({ err: `can't find user with id ${uid}` });
							return ;
						} else {
							User.destroy({ where: { id: uid } })
							.then((resu) => { this.out.json({ success: true }); })
							.catch((err) => { this.out.json({ err: err }); });
						};
					})
					.catch((err) => {
						this.out.json({ err: err });
					});
				})
				.catch((err) => {
					this.out.json({ err: err });
				});
			};
		};
	};
};

module.exports = (req, res) => {
	if (availableMethod.indexOf(req.method) == -1) {
		res.json({ err: `we doesn't support this method.` });
		return ;
	} else {
		let pars = (req.method == 'GET') 
				? req.query
				: (req.method == 'POST' || req.method == 'PUT') 
					? req.body 
					: {};

		let helper = new userManager(pars, req.session, res);
		helper[req.method]();
	};
};