'use strict';
require('dotenv').config({ silent: true });

const server = require('./lib/app');
const port = process.env.PORT || 3000
server.listen(port, function() {
    console.log('Free Contest Web Page is running on ' + port)
});
