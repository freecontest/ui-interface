# Define
~/ is the project folder (FC)

# To-do
Update dbUri in ~/.env to **localhost**

# Requirements
Backup database in AWS.
Reconfig username, password, database name in ~/.env

# Some commands
To run ui-interface
```
npm run interface
```

To clear all database (DANGER)
```
npm run sync setup
```

To add new admin with username (had registered in freecontest ui-interface)
```
npm run sync addAdmin [username]
```

**Example**
```
npm run sync addAdmin rknguyen
```
