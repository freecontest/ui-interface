const User = require('./db/user');
const bcrypt = require('bcrypt-nodejs')

const convert = async () => 
{
	const allUsers = await User.all();

	console.log('successfully fetched ' + allUsers.length + ' users');

	for (let user of allUsers)
	{
		user = user.dataValues;

		await User.update({
			password: bcrypt.hashSync(user.password)
		}, {
			where: {
				id: {
					$eq: user.id
				}
			}
		})

		console.log('update ' + user.username + ' successfully');
	}
}

convert();