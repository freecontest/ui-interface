var app = angular.module('app', ['ngRoute', 'ui.bootstrap', 'toastr', 'ngAnimate', 'ui.bootstrap.datetimepicker', 'oitozero.ngSweetAlert']); // use route, ui-bootstrap
app
.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
	$routeProvider
	.when('/home', {
		title: 'Trang chủ',
		shortUrl: 'home',
		controller: 'HomeController',
		templateUrl: './static/template/default/index.html'
	})
	.when('/login', {
		title: 'Đăng nhập',
		shortUrl: 'login',
		controller: 'LoginController',
		templateUrl: './static/template/default/login.html'
	})
	.when('/register', {
		title: 'Đăng ký',
		shortUrl: 'register',
		controller: 'RegisterController',
		templateUrl: './static/template/default/register.html'
	})
	.when('/updateInformation', {
		title: 'Cập nhật thông tin',
		shortUrl: 'update',
		controller: 'updateInformationController',
		templateUrl: './static/template/default/update.html'
	})
	.when('/post/:id', {
		title: 'Bài viết',
		shortUrl: 'post_',
		controller: 'PostIDControllers',
		templateUrl: './static/template/default/postwid.html'
	})
	.when('/user/:username', {
		title: '',
		shortUrl: 'profile',
		controller: 'ProfileController',
		templateUrl: './static/template/default/profile.html'
	})
	.when('/admin/edit/post/:id', {
		title: 'Sửa bài viết',
		shortUrl: 'editPost',
		controller: 'EditPostWIDController',
		templateUrl: './static/template/admin/editpost.html'
	})
	.when('/contests', {
		title: 'Contests',
		shortUrl: 'contests',
		controller: 'ContestsController',
		templateUrl: './static/template/default/contests.html'
	})
	.when('/admin/post', {
		title: `Đăng bài`,
		shortUrl: 'postmanager',
		controller: 'PostController',
		templateUrl: './static/template/admin/post.html'
	})
	.when('/admin/edit/contest/:id', {
		title: 'Sửa contest',
		shortUrl: 'editContets',
		controller: 'EditContestController',
		templateUrl: './static/template/admin/editcontest.html'
	})
	.when('/admin/add/contest', {
		title: 'Thêm contest',
		shortUrl: 'addcontest',
		controller: 'AddContestController',
		templateUrl: './static/template/admin/addcontest.html'
	})
	.when('/rating', {
		title: 'Ranking',
		shortUrl: 'ranking',
		controller: 'RankingController',
		templateUrl: './static/template/default/ranking.html'
	})
	.when('/contests/register/:id', {
		title: 'Đăng ký contest',
		shortUrl: 'contestreg',
		controller: 'RegContestController'
	})
	.when('/admin/user/manager', {
		title: 'Quản lý người dùng',
		shortUrl: 'usermanager',
		controller: 'UserManagerController',
		templateUrl: './static/template/admin/usermanager.html'
	})
	.when('/admin/user/edit/:userid', {
		title: 'Cập nhật thông tin',
		shortUrl: 'updateuser',
		controller: 'UpdateUserController',
		templateUrl: './static/template/admin/updateuser.html'
	})
	.when('/register/show/:cid', {
		title: 'Những người đã đăng ký',
		shortUrl: 'hasreg',
		controller: 'HasRegController',
		templateUrl: './static/template/default/hasreg.html'
	})
	.when('/autologin/:cid', {
		title: 'Tự động truy cập vào kỳ thi',
		shortUrl: 'preautologin',
		controller: 'AutoLoginController',
		templateUrl: './static/template/default/prelogintoCMS.html'
	})
	.when('/register/new/:cid', {
		title: 'Đăng ký tham gia contest',
		shortUrl: 'newreg',
		controller: 'RegisterContestController', 
		templateUrl: './static/template/default/newContestRegister.html'
	})
	.otherwise({
		redirectTo: '/home'
	});

	if (window.history && window.history.pushState){
		$locationProvider.html5Mode({
			enabled: true,
			rewriteLinks: true
		});
	} else {
		alert('Please use browser which support html5');
	}
}])
.run(['$rootScope', '$route', '$http', '$location', '$animate', 'toastr', 'FreeContest', '$timeout', '$routeParams', function ($rootScope, $route, $http, $location, $animate, toastr, FreeContest, $timeout, $routeParams) {
	$animate.enabled(false); // disabled ngAnimate for fix navbar

	// colors
	$rootScope.colors = ["dark-red", "black", "gray", "green", "cyan", "blue", "violet", "orange", "orange", "red", "red", "super-red"];
	$rootScope.titles = ["Headquarters", "Unrated", "Newbie", "Pupil", "Specialist", "Expert", "Candidate Master", "Master", "International Master", "Grandmaster", "International Grandmaster", "Legendary Grandmaster"]
	$rootScope.getColor = function(rating) {
	    if (0    <= rating && rating <= 1199) return 2;
	    if (1200 <= rating && rating <= 1399) return 3;
	    if (1400 <= rating && rating <= 1599) return 4;
	    if (1600 <= rating && rating <= 1899) return 5;
	    if (1900 <= rating && rating <= 2199) return 6;
	    if (2200 <= rating && rating <= 2299) return 7;
	    if (2300 <= rating && rating <= 2399) return 8;
	    if (2400 <= rating && rating <= 2599) return 9;
	    if (2600 <= rating && rating <= 2899) return 10;
	    if (2900 <= rating)                   return 11;
	}

	// set locale to vi
	moment.locale('vi');

	// some funny init
	$rootScope.routingChange = false;
	$rootScope.logined = false;
	$rootScope.user = {};
	$rootScope.moment = moment;

	// block scroll
	$rootScope.keys = {37: 1, 38: 1, 39: 1, 40: 1};
	$rootScope.preventDefault = (e) => {
		e = e || window.event;
		if (e.preventDefault)
			e.preventDefault();
		e.returnValue = false;
	}

	$rootScope.preventDefaultForScrollKeys = (e) => {
		if ($rootScope.keys[e.keyCode]) {
			$rootScope.preventDefault(e);
			return false;
		}
	}

	$rootScope.disableScroll = () => {
		if (window.addEventListener) // older FF
			window.addEventListener('DOMMouseScroll', $rootScope.preventDefault, false);
		window.onwheel = $rootScope.preventDefault; // modern standard
		window.onmousewheel = document.onmousewheel = $rootScope.preventDefault; // older browsers, IE
		window.ontouchmove  = $rootScope.preventDefault; // mobile
		document.onkeydown  = $rootScope.preventDefaultForScrollKeys;
	}

	$rootScope.enableScroll = () => {
		if (window.removeEventListener)
			window.removeEventListener('DOMMouseScroll', $rootScope.preventDefault, false);
		window.onmousewheel = document.onmousewheel = null;
		window.onwheel = null;
		window.ontouchmove = null;
		document.onkeydown = null;
	}

	$rootScope.$on('$routeChangeStart', function() {
		Pace.restart();
		clearInterval($rootScope.intervalComment)
		$rootScope.routingChange = true;
		$rootScope.disableScroll();

		$rootScope.noticeTime = null;
		$rootScope.intervalVar = null;
	});

	$rootScope.$on('$routeChangeSuccess', function() {
		$rootScope.curUrl = $route.current.shortUrl;
		document.title = ($rootScope.curUrl == 'profile'
						? $routeParams.username
						: $route.current.title)
						+ ' | Free Contest';
		$rootScope.routingChange = false;
		$rootScope.enableScroll();
	});

	$rootScope.$on('$viewContentLoaded', function(){
		$timeout(() => {
			$animate.enabled(true);
		}, 1000);
	});

	$rootScope.logOut = () => {
		FreeContest.logOut((res) => {
			if (res.err !== undefined) {
				toastr.error(res.err, 'Thông báo');
			} else {
				toastr.success('Đăng xuất thành công', 'Thông báo');
				if ($rootScope.curUrl !== 'home') {
					$location.path('/home');
				} else {
					$route.reload();
				};
			};
		});
	};

	$rootScope.notificationsEnabled = false;
	$rootScope.initNotifications = () => {
		if (window.Notification) {
			Notification.requestPermission(function(permission) {
				if (permission === 'granted') {
					notificationsEnabled = true;
				}
			});
		}
	}

	$rootScope.showNotification = () => {
		if (notificationsEnabled) {
			var notification = new Notification("Free Contest", {
				body : 'Kỳ thi đã bắt đầu, hãy tham gia ngay!!',
				icon : 'https://freecontest.xyz/static/img/favicon.png'
			});	
			setTimeout(function() { notification.close(); }, 5000);
		}
	}

	$rootScope.intervalVar = null;
	$rootScope.noticeTime = null;

	$rootScope.isInReminder = () =>
	{
		if ($rootScope.noticeTime === null)
		{
			clearInterval($rootScope.intervalVar)
		} 
		else 
		{
			let currentTime = (new Date()).getTime()
			if (currentTime > $rootScope.noticeTime)
			{
				$rootScope.showNotification();
				$route.reload();
				clearInterval($rootScope.intervalVar)
			}
		}
	}

	$rootScope.setReminder = (time) =>
	{
		$rootScope.noticeTime = (new Date(time)).getTime()
		$rootScope.intervalVar = setInterval($rootScope.isInReminder, 1000)
	}
}])
.constant('uiDatetimePickerConfig', {
	dateFormat: 'yyyy-MM-dd HH:mm:ss',
	defaultTime: '00:00:00',
	html5Types: {
		date: 'yyyy-MM-dd',
		'datetime-local': 'yyyy-MM-ddTHH:mm:ss.sss',
		'month': 'yyyy-MM'
	},
	initialPicker: 'date',
	reOpenDefault: false,
	enableDate: true,
	enableTime: true,
	buttonBar: {
		show: true,
		now: {
			show: true,
			text: 'Now',
			cls: 'btn-sm btn-default'
		},
		today: {
			show: true,
			text: 'Today',
			cls: 'btn-sm btn-default'
		},
		clear: {
			show: true,
			text: 'Clear',
			cls: 'btn-sm btn-default'
		},
		date: {
			show: true,
			text: 'Date',
			cls: 'btn-sm btn-default'
		},
		time: {
			show: true,
			text: 'Time',
			cls: 'btn-sm btn-default'
		},
		close: {
			show: true,
			text: 'Close',
			cls: 'btn-sm btn-default'
		},
		cancel: {
			show: false,
			text: 'Cancel',
			cls: 'btn-sm btn-default'
		}
	},
	closeOnDateSelection: true,
	closeOnTimeNow: true,
	appendToBody: false,
	altInputFormats: [],
	ngModelOptions: {},
	saveAs: false,
	readAs: false
});

app.config(['toastrConfig', function (toastrConfig) {
	angular.extend(toastrConfig, {
		allowHtml: false,
		closeButton: true,
		closeHtml: '<button>&times;</button>',
		extendedTimeOut: 1000,
		iconClasses: {
			error: 'toast-error',
			info: 'toast-info',
			success: 'toast-success',
			warning: 'toast-warning'
		},
		messageClass: 'toast-message',
		onHidden: null,
		onShown: null,
		onTap: null,
		progressBar: true,
		tapToDismiss: true,
		timeOut: 1500,
		titleClass: 'toast-title',
		toastClass: 'toast',
		positionClass: 'toast-bottom-right'
	});
}]);

app.controller('HomeController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', '$animate', 'FreeContest', '$sce', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, $animate, FreeContest, $sce) {
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
	});

	// posts
	$scope.Posts = [];
	$scope.PostLoading = true;
	FreeContest.getPosts((response) => {
		for (var i = 0; i < response.length; ++i) {
			let converter = new showdown.Converter({emoji: true});
			response[i].HTMLContent = $sce.trustAsHtml(converter.makeHtml(response[i].content));
			response[i].isopen = false;
			response[i].show = true;
			response[i].formLoading = false;
			response[i].fbShareLink = 'https://www.facebook.com/sharer/sharer.php?u=' + window.location.origin + '/post/' + response[i].id;
		}
		$scope.Posts = response;
		$scope.PostLoading = false;
	});

	$scope.delete = (post) => {
		post.formLoading = true;
		FreeContest.deletePost(post.id, (res) => {
			$timeout(() => {
				if (res.err !== undefined) {
					toastr.error('Có lỗi xảy ra', 'Thông báo');
				} else {
					post.show = false;
					toastr.success('Xóa bài viết thành công', 'Thông báo');
				};

				post.formLoading = false;
			}, 500);
		});
	};

	// ranking
	$scope.RankLoading = true;
	$scope.topten = [];
	FreeContest.getUsers((res) => {
		$scope.RankLoading = false;
		for (var i = 0; i < res.length; ++i)
			res[i].rank = i + 1;
		$scope.topten = res;
	}, 10);

	// contests
	$scope.UpcomingContests = [];
	$scope.RecentContests = [];
	$scope.PresentContest = [];
	$scope.ContestLoading = true;

	FreeContest.getContest((contests) => {
		$scope.contests = contests;
		for (var i = 0; i < contests.length; ++i) {
			let startedTime = (new Date(contests[i].startTime)).getTime();
			let endTime = (new Date(contests[i].endTime)).getTime();
			let currentTime = new Date().getTime();

			if (endTime < currentTime) {
				$scope.RecentContests.push(contests[i]);
			} else
			if (startedTime > currentTime) {
				$scope.UpcomingContests.push(contests[i]);
			} else
			if (startedTime <= currentTime && currentTime <= endTime) {
				$scope.PresentContest.push(contests[i]);
			};

			contests[i].writtersJOIN = contests[i].writters.join(', ');
		};

		$scope.RecentContests.reverse();
		$scope.ContestLoading = false;
	}, 10);
}]);

app.controller('ProfileController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', '$animate', 'FreeContest', '$sce', '$routeParams', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, $animate, FreeContest, $sce, $routeParams) {
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
	});

	$scope.UserLoading = true;
	$scope.ContestLoading = true;

	$scope.username = $routeParams.username;
	$scope.User = {};

	$scope.maxRating = -1

	FreeContest.getUser($scope.username, (res) => {
		if (res.err !== undefined) {
			toastr.error(`Không tồn tại người dùng ${$scope.username}`, 'Thông báo');
			$location.path('/home');
		} else {
			$scope.User = res;

			for (let log of $scope.User.logsUpdateRating) {
				$scope.maxRating = Math.max($scope.maxRating, log.rating)
			}

			$scope.UserLoading = false;
			$scope.loadGraph();
			$scope.loadReged();
		};
	});

	$scope.loadGraph = () => {
		var m, w, F, A, M, _, S, b, L, C, E, k, P, B, T, D, I, R, X, Y, J = createjs;
	    let O = 2592e3,
	        W = 100,
	        G = 300,
	        H = 50,
	        j = 5;
	    var q = document.getElementById("ratingStatus");
	    let N = q.width - H - 10,
	        z = q.height - j - 5;
	    var K = document.getElementById("ratingGraph");
	    let Q = K.width - H - 10,
	        U = K.height - j - 30,
	        V = 80,
	        Z = 20,
	        tt = "12px Lato",
	        et = 2010,
	        it = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
	        rt = 31536e3,
	        at = 100,
	        nt = [
	            [0   , "gray",    .1],
	            [1200, "green",   .1],
	            [1400, "#03A89E", .1],
	            [1600, "blue",    .1],
	            [1900, "#a0a",    .1],
	            [2100, "#FF8C00", .1],
	            [2300, "#ffbb55", .1],
	            [2400, "#ff7777", .1],
				[2600, "#ff3333", .1],
				[3000, "#aa0000", .1]
	        ],
	        st = 3200,
	        ot = 3,
	        ht = 20,
	        ct = 30

			function e(t, e) {
	        var i = e.getAttribute("width"),
	            r = e.getAttribute("height");
	        window.devicePixelRatio && (e.setAttribute("width", Math.round(i * window.devicePixelRatio)), e.setAttribute("height", Math.round(r * window.devicePixelRatio)), t.scaleX = t.scaleY = window.devicePixelRatio), e.style.maxWidth = i + "px", e.style.maxHeight = r + "px", e.style.width = e.style.height = "100%", t.enableMouseOver()
	    }

	    function i(t) {
	        return s = new J.Shape, t.addChild(s), s
	    }

	    function r(e, i, r, a) {
	        return t = new J.Text("", a, "#000"), t.x = i, t.y = r, t.textAlign = "center", t.textBaseline = "middle", e.addChild(t), t
	    }

	    function a() {
	        function t(t) {
	            y(), w.update()
	        }
	        if (L = rating_history.length, 0 != L) {
	            m = new J.Stage("ratingGraph"), w = new J.Stage("ratingStatus"), e(m, K), e(w, q), C = 1e11, E = 0, k = 1e4, P = 0;
	            for (var i = 0; L > i; i++) C = Math.min(C, rating_history[i][0]), E = Math.max(E, rating_history[i][0]), k = Math.min(k, rating_history[i][1]), P = Math.max(P, rating_history[i][1]);
	            C -= O, E += O, k = Math.min(1500, Math.max(0, k - W)), P += G, h(), c(), m.update(), d(), w.update(), J.Ticker.setFPS(60), J.Ticker.addEventListener("tick", t)
	        }
	    }

	    function n(t, e, i) {
	        return (t - e) / (i - e)
	    }

	    function o(t) {
	        for (var e = nt.length - 1; e >= 0; e--)
	            if (t >= nt[e][0]) return nt[e];
	        return [-1, "#000000", .1]
	    }

	    function h() {
	        function e(e, i) {
	            t = new J.Text(e, tt, "#000"), t.x = 40, t.y = j + i, t.textAlign = "right", t.textBaseline = "middle", m.addChild(t)
	        }

	        function r(e, i, r) {
	            t = new J.Text(e, tt, "#000"), t.x = H + i, t.y = j + U + 2 + r, t.textAlign = "center", t.textBaseline = "top", m.addChild(t)
	        }
	        F = i(m), F.x = H, F.y = j, F.alpha = .3, A = i(m), A.x = H, A.y = j;
	        for (var a = 0, s = nt.length - 1; s >= 0; s--) {
	            var o = U - U * n(nt[s][0], k, P);
	            o > 0 && U > a && (a = Math.max(a, 0), F.graphics.f(nt[s][1]).r(0, a, Q, Math.min(o, U) - a)), a = o
	        }
	        for (var s = 0; P >= s; s += at)
	            if (s >= k) {
	                var h = U - U * n(s, k, P);
	                e(String(s), h), A.graphics.s("#FFF").ss(.5), 2e3 == s && A.graphics.s("#000"), A.graphics.mt(0, h).lt(Q, h)
	            }
	        A.graphics.s("#FFF").ss(.5);
	        for (var c = 6, s = 3; s >= 1; s--) rt * s + 5184e3 >= E - C && (c = s);
	        for (var d = !0, s = et; 3e3 > s; s++) {
	            for (var g = !1, u = 0; 12 > u; u += c)
	                if (month = ("00" + (u + 1)).slice(-2), unix = Date.parse(String(s) + "-" + month + "-01 00:00:00") / 1e3, C < unix && unix < E && (x = Q * n(unix, C, E), 0 == u || d ? (r(it[u], x, 0), r(String(s), x, 13), d = !1) : r(it[u], x, 0), A.graphics.mt(x, 0).lt(x, U)), unix > E) {
	                    g = !0;
	                    break
	                }
	            if (g) break
	        }
	        A.graphics.s("#888").ss(1.5).rr(0, 0, Q, U, 2)
	    }

	    function c() {
	        function t(t) {
	            S[t.target.i].scaleX = S[t.target.i].scaleY = 1.2, m.update(), l(rating_history[t.target.i], !0)
	        }

	        function e(t) {
	            S[t.target.i].scaleX = S[t.target.i].scaleY = 1, m.update()
	        }
	        M = new J.Container, m.addChild(M), M.shadow = new J.Shadow("rgba(0,0,0,0.3)", 1, 2, 3), _ = i(M), b = i(M), S = new Array;
	        for (var a = 0, s = 0; L > s; s++) rating_history[a][1] < rating_history[s][1] && (a = s);
	        for (var s = 0; L > s; s++) S.push(i(M)), S[s].graphics.s("#FFF"), s == a && S[s].graphics.s("#000"), S[s].graphics.ss(.5).f(o(rating_history[s][1])[1]).dc(0, 0, 3.5), S[s].x = H + Q * n(rating_history[s][0], C, E), S[s].y = j + (U - U * n(rating_history[s][1], k, P)), S[s].i = s, hitArea = new J.Shape, hitArea.graphics.f("#000").dc(1.5, 1.5, 6), S[s].hitArea = hitArea, S[s].addEventListener("mouseover", t), S[s].addEventListener("mouseout", e);
	        var h = 80;
	        (C + E) / 2 < rating_history[a][0] && (h = -80);
	        var c = S[a].x + h,
	            d = S[a].y - 16;
	        b.graphics.s("#FFF").mt(S[a].x, S[a].y).lt(c, d), b.graphics.s("#888").f("#FFF").rr(c - 40, d - 10, V, Z, 2), b.i = a, highest_text = r(m, c, d, "12px Lato"), highest_text.text = "Highest: " + rating_history[a][1], b.addEventListener("mouseover", t), b.addEventListener("mouseout", e);
	        for (var g = 0; 2 > g; g++) {
	            0 == g ? _.graphics.s("#AAA").ss(2) : _.graphics.s("#FFF").ss(.5), _.graphics.mt(S[0].x, S[0].y);
	            for (var s = 0; L > s; s++) _.graphics.lt(S[s].x, S[s].y)
	        }
	    }

	    function d() {
	        B = i(w), T = r(w, 130, j + z / 2, "48px 'Squada One'"), D = r(w, 210, j + z / 2, "16px Lato"), I = r(w, 250, j + z / 4, "14px Lato"), R = r(w, 250, j + z / 1.6, "20px Lato"), I.textAlign = R.textAlign = "left", R.maxWidth = N - 200 - 10, hitArea = new J.Shape, hitArea.graphics.f("#000").r(0, -12, R.maxWidth, 24), X = new Array;
	        for (var t = 0; ht > t; t++) X.push(r(w, 0, 0, "64px Lato")), X[t].visible = !1;
	        l(rating_history[0], !1)
	    }

	    function g(t) {
	        for (var e = nt[nt.length - 1][0] + at, i = nt.length - 1; i >= 0; i--) {
	            if (t >= nt[i][0]) return (t - nt[i][0]) / (e - nt[i][0]);
	            e = nt[i][0]
	        }
	        return 0
	    }

	    function u(t) {
	        var e = ["th", "st", "nd", "rd"],
	            i = t % 100;
	        return t + (e[(i - 20) % 10] || e[i] || e[0])
	    }

	    function l(t, e) {
	        var i = new Date(t[0] * 1e3),
	            r = t[1],
	            a = t[2],
	            n = t[3],
	            s = o(r),
	            h = s[1],
	            c = s[2];
	        if (B.graphics.c().s(h).ss(1).rr(H, j, N, z, 2), T.text = r, T.color = h, D.text = u(a), I.text = i.toLocaleDateString(), R.text = n, e) {
	            var d = parseInt(17 * Math.pow(g(r), 2) + ot);
	            p(d, h, c, r)
	        }
	        Y = t[4]
	    }

	    function f(t, e, i, r, a, n) {
	        t.x = e, t.y = i, ang = Math.random() * Math.PI * 2, speed = 4 * Math.random() + 4, t.vx = Math.cos(ang) * speed, t.vy = Math.sin(ang) * speed, t.rot_speed = 20 * Math.random() + 10, t.life = ct, t.visible = !0, t.color = r, n ? t.text = "笘�" : t.text = "@", t.alpha = a
	    }

	    function p(t, e, i, r) {
	        for (var a = 0; ht > a; a++) t > a ? f(X[a], T.x, T.y, e, i, r >= st) : (X[a].life = 0, X[a].visible = !1)
	    }

	    function v(t) {
	        return t.life <= 0 ? void(t.visible = !1) : (t.x += t.vx, t.vx *= .9, t.y += t.vy, t.vy *= .9, t.life--, t.scaleX = t.scaleY = t.life / ct, void(t.rotation += t.rot_speed))
	    }

	    function y() {
	        for (var t = 0; ht > t; t++) X[t].life > 0 && v(X[t])
	    }

		var _xx = []
		for (let log of $scope.User.logsUpdateRating) {
			_xx.push([ Math.round((new Date(log.startTime)).getTime() / 1e3), log.rating, log.rank, log.contestName, 1212 ])
		}

		window.rating_history = _xx
		a()
	}


	// reged contests
	$scope.RegisteredContests = [];
	$scope.loadReged = () => {
		FreeContest.getContestX($scope.User.id, (contests) => {
			for (var i = 0; i < contests.length; ++i) {
				let startedTime = (new Date(contests[i].startTime)).getTime();
				let endTime = (new Date(contests[i].endTime)).getTime();
				let currentTime = new Date().getTime();

				if (endTime < currentTime) {
					contests[i].recent = true;
				} else
				if (startedTime > currentTime) {
					contests[i].upcoming = true;
				} else
				if (startedTime <= currentTime && currentTime <= endTime) {
					contests[i].present = true;
				};

				if (contests[i].isRegistered) {
					$scope.RegisteredContests.unshift(contests[i]);
				};
			};
			$scope.ContestLoading = false;
		});
	};
}]);

app.controller('RankingController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', '$animate', 'FreeContest', '$sce', '$routeParams', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, $animate, FreeContest, $sce, $routeParams) {
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
	});

	$scope.ranking = [];
	$scope.RankLoading = true;
	$scope.offset = 0
	FreeContest.getUsers((res) => {
		for (var i = 0; i < res.length; ++i)
			res[i].rank = i + 1;
		$scope.ranking = res;
		$scope.RankLoading = false;
	}, 100, 0);

	$scope.loadMore = () => {
		$scope.offset += 100
		$scope.RankLoadingBtn = true;
		FreeContest.getUsers((res) => {
			for (var i = 0; i < res.length; ++i)
				res[i].rank = $scope.ranking.length + i + 1;
			$scope.ranking = $scope.ranking.concat(res);
			$scope.RankLoadingBtn = false;
		}, 100, $scope.offset);
	}
}]);

app.controller('HasRegController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', '$animate', 'FreeContest', '$sce', '$routeParams', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, $animate, FreeContest, $sce, $routeParams) {
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
	});

	$scope.cid = $routeParams.cid;
	$userLoading = true;

	$scope.users = [];
	$scope.contest = null;
	FreeContest.getUsersRegistered($scope.cid, (users) => {
		if (users.err !== undefined) {
			toastr.error('Có lỗi xảy ra', 'Thông báo');
			$location.path('/home');
		};

		for (var i = 0; i < users.length; ++i) {
			users[i].stt = (i + 1);
		};

		$scope.users = users;
	});
}]);

app.controller('ContestsController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', '$animate', 'FreeContest', '$sce', '$routeParams', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, $animate, FreeContest, $sce, $routeParams) {
	// yeu cau quyen notify
	$rootScope.initNotifications();

	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
	});

	$scope.contests = [];
	$scope.UpcomingContests = [];
	$scope.RecentContests = [];
	$scope.PresentContest = [];
	$scope.RegisteredContests = [];

	$scope.ContestLoading = true;

	FreeContest.getContest((contests) => {
		$scope.contests = contests;
		for (var i = 0; i < contests.length; ++i) {
			let startedTime = (new Date(contests[i].startTime)).getTime();
			let endTime = (new Date(contests[i].endTime)).getTime();
			let currentTime = new Date().getTime();

			if (endTime < currentTime) {
				$scope.RecentContests.push(contests[i]);
				contests[i].recent = true;
			} else
			if (startedTime > currentTime) {
				$scope.UpcomingContests.push(contests[i]);
				contests[i].upcoming = true;
			} else
			if (startedTime <= currentTime && currentTime <= endTime) {
				$scope.PresentContest.push(contests[i]);
				contests[i].present = true;
			};

			contests[i].writtersJOIN = contests[i].writters.join(', ');
			contests[i].reging = false;

			if (contests[i].isRegistered) {
				$scope.RegisteredContests.unshift(contests[i]);
			};
		};

		$scope.RecentContests.reverse();

		// bao thong tin moi khi contest bat dau
		if ($scope.UpcomingContests.length > 0) {
			$rootScope.setReminder((new Date($scope.UpcomingContests[0].startTime)).getTime())
		}

		$scope.ContestLoading = false;
	});

	// $scope.RegedContests = [];
	// FreeContest.getContestReged((res) => {
	// 	$scope.RegedContests = res;
	// });

	$scope.registerContest = (contest) => {
		if (!$rootScope.logined) {
			toastr.error('Bạn cần phải đăng nhập trước', 'Thông báo');
		} else {
			if (contest.reging) {
				toastr.error('Đang đăng ký, bạn đợi tí nhé', 'Thông báo');
			} else {
				contest.reging = true;
				FreeContest.registerContest(contest.id, (res) => {
					if (res.err !== undefined) {
						toastr.error('Có lỗi xảy ra', 'Thông báo');
					} else {
						toastr.success('Đăng ký thành công', 'Thông báo');
					};
					$route.reload();
					contest.reging = false;
				});
			}
		};
	};

	$scope.showRanking = (contest) => {
		toastr.info('Coming soon...', 'Thông báo');
	};
}]);

app.controller('LoginController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'FreeContest', 'toastr', function ($rootScope, $scope, $http, $route, $location, $timeout, FreeContest, toastr) {
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
		if (res.logined == true) {
			toastr.error('Bạn cần đăng xuất trước', 'Thông báo');
			$location.path('/home');
		};
	});

	// some init
	$scope.formLoading = false;

	$scope.login = () => {
		let username = $scope.username,
			password = $scope.password;

		$scope.formLoading = true;

		FreeContest.signIn(username, password, (res) => {
			$scope.formLoading = false;

			if (res.err !== undefined) {
				toastr.error('Không tồn tại tài khoản hoặc sai mật khẩu', 'Thông báo');
				return ;
			} else {
				toastr.success('Đăng nhập thành công', 'Thông báo');
				$location.path('/home');
			};
		});
	};
}]);

app.controller('AutoLoginController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'FreeContest', 'toastr', '$routeParams', function ($rootScope, $scope, $http, $route, $location, $timeout, FreeContest, toastr, $routeParams) {
	$scope.UserLoading = true;
	$scope.loadingMessage = '';

	// we have 4 stage
	// check login
	// check contest is valid
	// get tmp password
	// auto login

	$scope.checkLogined = false;
	$scope.checkContest = false;
	$scope.catchedTmpPassword = false;

	$scope.cid = $routeParams.cid

	FreeContest.getSession((res) => {
		$scope.loadingMessage = 'Đang kiểm tra trạng thái đăng nhập';

		$rootScope.logined = res.logined;
		$rootScope.user = res;
		if (res.logined == false) {
			toastr.error('Bạn cần đăng nhập trước', 'Thông báo');
			$location.path('/login');
		} else {
			$scope.checkLogined = true;
			$scope.checkContestIsValid();
		}
	});

	$scope.checkContestIsValid = () =>
	{
		$scope.loadingMessage = 'Đang kiểm tra tính hợp lệ của contest'
		
		let cid = $scope.cid
		FreeContest.getContestWID(cid, (contest) => {
			if (contest.length == 0 || contest.err !== undefined) {
				toastr.error(`Không tìm thấy contest với id ${$scope.cid}`, 'Thông báo');
				$location.path('/contests');
			} else {
				contest = contest[0]

				let startTime = new Date(contest.startTime)
				let endTime = new Date(contest.endTime)
				let current = new Date()

				if (startTime <= current && current <= endTime) 
				{
					$scope.checkContest = true;
					$scope.getTmpPassword();
				}
				else 
				{
					toastr.error(`${contest.name} chưa diễn ra`, 'Thông báo');
					$location.path('/contests');
				}
			}
		})
	}
	
	$scope.getTmpPassword = () =>
	{
		$scope.loadingMessage = 'Đang chuẩn bị mật khẩu để đăng nhập'

		let cid = $scope.cid
		let uid = $rootScope.user.uid

		FreeContest.getTmpPassword(uid, cid, (res) => {
			if (res.success === undefined || res.success === null)
			{
				toastr.error(`Không lấy được mật khẩu, hãy liên hệ với Free Contest để được tư vấn`, 'Thông báo');
				$location.path('/contests');
			}
			else
			{
				$scope.catchedTmpPassword = true

				let username = $rootScope.user.username
				let tmpPassword = res.tmpPassword

				$scope.autoLogin(username, tmpPassword)
			}
		})
	}

	$scope.autoLogin = async (username, password) =>
	{
		$scope.loadingMessage = 'Đang đăng nhập vào tài khoản ' + username

		try {
			let cms = new FreeContest_CMS(FC_CMS_URL);
		    let result = await cms.login(username, password);

		    if (result) {
		        window.location.replace(FC_CMS_URL);
		    } else {
		        toastr.error(`Có lỗi khi đăng nhập`, 'Thông báo');
				$location.path('/contests');
		    }   
		}
		catch(e) 
		{
			console.log(e)
			toastr.error(`Có lỗi khi đăng nhập`, 'Thông báo');
			$location.path('/contests');
		}
	}

	// Created by Tuan Kiet Ho (tuankiet65) of Free Contest Server Team

	const FC_CMS_URL = "https://code.freecontest.xyz/"

	class FreeContest_CMS {
	    constructor (cms_url) {
	        this.cms_url = cms_url
	    }
	    
	    async get_xsrf_token() {
		    let response = await fetch(`${this.cms_url}`, {
			    method: "GET",
			    credentials: "include"
		    })
		    
	        let text = await response.text()
	        let parser = new DOMParser()
	        let doc = parser.parseFromString(text, "text/html")
	        let xsrf_token = doc.getElementsByName("_xsrf")[0].value
	        
	        return xsrf_token;
	    }
	    
	    async logout() {
	        let xsrf_token = await this.get_xsrf_token()
	        
	        let params = new URLSearchParams()
	        params.append("_xsrf", xsrf_token)
	        
		    let response = await fetch(`${this.cms_url}/logout`, {
			    method: "POST",
			    body: params,
			    credentials: "include"
		    })
		    
	        return true;
	    }
	    
	    async login(username, password) {
	        let xsrf_token = await this.get_xsrf_token()
	        
	        let params = new URLSearchParams()
	        params.append("_xsrf", xsrf_token)
	        params.append("username", username)
	        params.append("password", password)
	        
		    let response = await fetch(`${this.cms_url}/login`, {
			    method: "POST",
			    body: params,
			    credentials: "include"
		    })
		    
		    let final_url = response.url
		    if (final_url.includes("?login_error=true")) return false;
	        return true;
	    }    
	}

	async function cms_logout() {
	    let cms = new FreeContest_CMS(FC_CMS_URL);
	    let result = await cms.logout();
	    
	    window.location.replace(FC_CMS_URL);
	}
}])

app.controller('RegisterController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'FreeContest', 'toastr', function ($rootScope, $scope, $http, $route, $location, $timeout, FreeContest, toastr) {
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
		if (res.logined == true) {
			toastr.error('Bạn cần đăng xuất trước', 'Thông báo');
			$location.path('/home');
		};
	});

	// some init
	$scope.formLoading = false;

	$scope.register = () => {
		let name = $scope.name,
			schoolName = $scope.schoolName,
			yob = $scope.yob,
			username = $scope.username,
			password = $scope.password,
			repassword = $scope.repassword;

		if (repassword !== password) {
			toastr.error('Mật khẩu nhập lại không khớp', 'Thông báo');
		} else {
			$scope.formLoading = true;

			FreeContest.register(name, schoolName, yob, username, password, (res) => {
				if (res.err !== undefined) {
					toastr.error(res.err, 'Thông báo');
				} else {
					toastr.success('Đăng ký thành công, bây giờ bạn có thể đăng nhập', 'Thông báo');
					$location.path('/login');
				};

				$scope.formLoading = false;
			});
		};
	};
}]);

app.controller('RegisterContestController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', '$animate', 'FreeContest', '$routeParams', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, $animate, FreeContest, $routeParams) {
	$scope.validating = true;
	$scope.cid = $routeParams.cid;
	$scope.displayOnRanking = true;
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;

		if (res.logined == false) {
			toastr.error('Bạn cần đăng nhập trước', 'Thông báo');
			return $location.path('/login');
		}
		
		let contestCond = {
			cid: $scope.cid
		}
		FreeContest.getSingleContest(contestCond, function(contest) {
			if (contest.err) {
				toastr.error('Đã có lỗi xảy ra, vui lòng báo cho tình nguyện viên để được khắc phục');
				return $location.path('/home');
			}

			if (contest.hasRegistered) {
				toastr.error('Bạn đã đăng ký contest này rồi');
				return $location.path('/home');
			}

			$scope.contest = contest;
			$scope.displayOnRanking = (contest.code == 1 ? false : !contest.IsDuring);
			$scope.validating = false;
		})
	});

	$scope.clicked = function(){
		$scope.displayOnRanking = !$scope.displayOnRanking;
	}

	$scope.registering = false;
	$scope.regThisContest = function() {
		$scope.registering = true;
		let params = {
			cid: $scope.cid,
			displayOnRanking: $scope.displayOnRanking
		}
		FreeContest.regSingleContest(params, function(res) {
			if (res.err) {
				toastr.error(res.err);
				return $location.path('/contests');
			}

			$scope.registering = false;
			toastr.success('Đăng ký thành công');
			return $location.path('/contests');
		})
	}
}]);

app.controller('updateInformationController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', '$animate', 'FreeContest', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, $animate, FreeContest) {
	$scope.UserLoading = true;

	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
		if (res.logined == false) {
			toastr.error('Bạn cần đăng nhập trước', 'Thông báo');
			$location.path('/login');
		};
		$scope.name = res.name;
		$scope.schoolName = res.schoolName;
		$scope.yob = res.yob;
		$scope.UserLoading = false;
		$scope.showOnRanking = res.showOnRanking;
	});

	$scope.password = null;
	$scope.repassword = null;

	$scope.formLoading1 = false;
	$scope.formLoading2 = false;

	$scope.updateInfo = () => {
		let name = $scope.name,
			schoolName = $scope.schoolName,
			yob = $scope.yob,
			password = $scope.password,
			repassword = $scope.repassword,
			showOnRanking = !$scope.showOnRanking;

		if (password !== null && password !== '') {
			if (password !== repassword) {
				toastr.error('Mật khẩu nhập lại không khớp', 'Thông báo');
			} else {
				$scope.formLoading2 = true;

				FreeContest.updateWithP(name, schoolName, yob, password, showOnRanking, (res) => {
					if (res.err !== undefined) {
						toastr.error('Có lỗi xảy ra', 'Thông báo');
					} else {
						toastr.success('Cập nhật thông tin thành công', 'Thông báo');
						$route.reload();
					};

					$scope.formLoading2 = false;
				});
			};
		} else {
			$scope.formLoading2 = true;

			FreeContest.updateWithOutP(name, schoolName, yob, showOnRanking, (res) => {
				if (res.err !== undefined) {
					toastr.error('Có lỗi xảy ra', 'Thông báo');
				} else {
					toastr.success('Cập nhật thông tin thành công', 'Thông báo');
					$route.reload();
				};

				$scope.formLoading2 = false;
			});
		};
	};

	$scope.updateAvt = () => {
		let email = $scope.email;
		$scope.formLoading1 = true;

		FreeContest.updateAvt(email.toLowerCase(), (res) => {
			if (res.err !== undefined) {
				toastr.error('Có lỗi xảy ra', 'Thông báo');
			} else {
				toastr.success('Cập nhật avatar thành công', 'Thông báo');
				$route.reload();
			};

			$scope.formLoading1 = false;
		});
	};
}]);

app.controller('UpdateUserController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', 'FreeContest', '$sce', '$routeParams', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, FreeContest, $sce, $routeParams) {
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
		if (res.logined == false) {
			toastr.error('Bạn cần đăng nhập trước', 'Thông báo');
			$location.path('/login');
		};
		if (res.role !== 'admin') {
			toastr.error('Bạn không có quyền truy cập', 'Thông báo');
			$location.path('/home');
		};

		$scope.UserLoad();
	});

	$scope.userid = $routeParams.userid;
	$scope.UserLoading = true;

	$scope.UserLoad = () => {
		$http.get(`api/admin/users?uid=${$scope.userid}`).then(res => res.data).then((res) => {
			if (res.err !== undefined || res.length == 0) {
				toastr.error('Không tìm thấy người dùng', 'Thông báo');
				$location.path('/home');
			} else {
				$scope.username = res[0].username;
				$scope.password = res[0].password;
				$scope.role = res[0].role;
				$scope.color = res[0].color;
				$scope.rating = res[0].rating;
			};
			$scope.UserLoading = false;
		});
	};

	$scope.update = () => {
		$scope.formLoading = true;

		let username = $scope.username,
			password = $scope.password,
			role = $scope.role,
			color = $scope.color,
			rating = $scope.rating;

		$http.put('api/admin/users', {
			uid: $scope.userid,
			username: username,
			password: password,
			role: role,
			color: color,
			rating: rating
		}).then(res => res.data).then((res) => {
			if (res.err !== undefined) {
				toastr.error('Có lỗi xảy ra', 'Thông báo');
			} else {
				toastr.success('Cập nhật thành công', 'Thông báo');
				$route.reload();
			};

			$scope.formLoading = false;
		});
	};
}]);

app.controller('UserManagerController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', 'FreeContest', '$sce', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, FreeContest, $sce) {
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
		if (res.logined == false) {
			toastr.error('Bạn cần đăng nhập trước', 'Thông báo');
			$location.path('/login');
		};
		if (res.role !== 'admin') {
			toastr.error('Bạn không có quyền truy cập', 'Thông báo');
			$location.path('/home');
		};
	});

	$scope.users = [];
	$scope.UserLoading = true;
	$http.get('api/admin/users').then(res => res.data).then((res) => {
		for (var i = 0; i < res.length; ++i)
			res[i].stt = (i + 1);
		$scope.users = res;
		$scope.UserLoading = false;
	});
}]);

app.controller('AddContestController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', 'FreeContest', '$sce', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, FreeContest, $sce) {
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
		if (res.logined == false) {
			toastr.error('Bạn cần đăng nhập trước', 'Thông báo');
			$location.path('/login');
		};
		if (res.role !== 'admin') {
			toastr.error('Bạn không có quyền truy cập', 'Thông báo');
			$location.path('/home');
		};
	});

	$scope.name = "Free Contest "
	$scope.duration = 3;
	$scope.writters = "xuanquang1999";
	$scope.startTime = {
		date: new Date((new Date()).toDateString())
	};
	$scope.endTime = {
		date: new Date((new Date()).toDateString())
	};

	$scope.formLoading = false;

	$scope.add = () => {
		$scope.formLoading = true;

		let name = $scope.name,
			startTime = $scope.startTime.date,
			endTime = $scope.endTime.date,
			duration = $scope.duration,
			writters = $scope.writters.split(',');

		FreeContest.addContest(name, startTime, endTime, duration, writters,
		(res) => {
			if (res.err !== undefined) {
				toastr.error('Có lỗi xảy ra', 'Thông báo');
			} else {
				toastr.success('Thêm contest thành công', 'Thông báo');
				$location.path('/contests');
			};

			$scope.formLoading = false;
		});
	};

	$scope.openCalendar = function(e, picker) {
		$scope[picker].open = true;
	};
}]);

app.controller('PostController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', 'FreeContest', '$sce', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, FreeContest, $sce) {
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
		if (res.logined == false) {
			toastr.error('Bạn cần đăng nhập trước', 'Thông báo');
			$location.path('/login');
		};
		if (res.role !== 'admin') {
			toastr.error('Bạn không có quyền truy cập', 'Thông báo');
			$location.path('/home');
		};
	});

	$scope.updatePreview = () => {
		let converter = new showdown.Converter({emoji: true});
		$scope.markdownPreview = $sce.trustAsHtml(converter.makeHtml($scope.content));
	};

	$scope.formLoading = false;
	$scope.markdownPreview = $sce.trustAsHtml(
		'<p>Hello.</p> <ul><li>This is markdown.</li><li>It is fun</li><li>Love it or leave it.</li></ul>'
	);

	$scope.Post = () => {
		let title = $scope.title,
			content = $scope.content;

		$scope.formLoading = true;

		FreeContest.createPost(title, content, (res) => {
			if (res.err !== undefined) {
				toastr.error('Có lỗi xảy ra', 'Thông báo');
			} else {
				toastr.success('Đăng bài thành công', 'Thông báo');
				$location.path('/home');
			};

			$scope.formLoading = false;
		});
	};
}]);

app.controller('PostIDControllers', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', 'FreeContest', '$sce', '$routeParams', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, FreeContest, $sce, $routeParams) {
	$scope.postid = $routeParams.id;
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;
	});

	$scope.loadingPost = true

	$scope.fullComment = false
	$scope.justNewest = false

	$scope.offset = 0
	$scope.limit = 10

	$scope.Posts = [];
	FreeContest.getPostWId($scope.postid, (res) => {
		if (res.err !== undefined || res.length == 0) {
			toastr.error('Có lỗi xảy ra', 'Thông báo');
			$location.path('/home');
		} else {
			for (var i = 0; i < res.length; ++i) {
				let converter = new showdown.Converter({emoji: true});
				res[i].HTMLContent = $sce.trustAsHtml(converter.makeHtml(res[i].content));
				$scope.AcntComment = res[i].cntComment
			}
			$scope.Posts = res;

			$scope.loadingPost = false

			$rootScope.intervalComment = setInterval(function() {
				let postID = $scope.postid
				let off = 0
				let lim = $scope.limit
				$http.get(`/api/actions?pid=${postID}&method=comment&limit=${lim}&offset=${off}`).then(r => r.data).then((x) => {
					let res = x.comments
					$scope.loadingComment = false
					if ($scope.justNewest) {
						$scope.justNewest = false
					} else {
						$scope.AcntComment = x.cntComment
						$scope.allComments = res
					}
				})
			}, 5000)
		};
	});

	$scope.content = ''
	$scope.cntEdit = 0
	$scope.deleteLogs = []

	$scope.comment = () => {
		$scope.commenting = true

		let content = $scope.content
		let postID = $scope.postid
		$http.post('/api/actions', {
			content: content,
			pid: postID,
			method: 'comment'
		}).then(res => res.data).then((res) => {
			$scope.commenting = false
			$scope.content = ''
			if (res.err !== undefined) {
				toastr.error('Có lỗi xảy ra', 'Thông báo');
			} else {
				toastr.success('Viết bình luận thành công', 'Thông báo')
			}
		})
	}

	$scope.loadingComment = true
	$scope.loadComment = () => {
		let postID = $scope.postid
		let off = 0
		let lim = $scope.limit
		$http.get(`/api/actions?pid=${postID}&method=comment&limit=${lim}&offset=${off}`).then(r => r.data).then((x) => {
			let res = x.comments
			$scope.loadingComment = false
			if ($scope.cntEdit == 0) {
				$scope.allComments = res
			}
		})
	}
	$scope.loadComment()

	$scope.deleteComment = (com) => {
		let comID = com.id
		com.deleting = true
		$http.post(`/api/actions`, {
			cid: comID,
			method: 'delete_comment'
		}).then(r => r.data).then((res) => {
			com.deleting = false
			com.deleted = true
			if (res.err !== undefined) {
				toastr.error('Có lỗi xảy ra', 'Thông báo');
			} else {
				$scope.deleteLogs.push(comID)
				toastr.success('Xoá bình luận thành công', 'Thông báo')
			}
		})
	}

	$scope.openModeEditComment = (com) => {
		$scope.cntEdit += 1
		clearInterval($rootScope.intervalComment)
		com.newComment = com.content
		com.editMode = true
	}

	$scope.editComment = (com) => {
		com.editing = true
		$http.put(`/api/actions`, {
			method: 'comment',
			cid: com.id,
			content: com.newComment
		}).then(r => r.data).then((res) => {
			if (res.err !== undefined) {
				toastr.error('Có lỗi xảy ra', 'Thông báo');
			} else {
				com.content = com.newComment
				com.updatedAt = new Date()
				$scope.offModeEditComment(com)
				toastr.success('Cập nhật bình luận thành công', 'Thông báo')
			}
			com.editing = false
		})
	}

	$scope.offModeEditComment = (com) => {
		$scope.cntEdit -= 1
		clearInterval($rootScope.intervalComment)
		$rootScope.intervalComment = setInterval(function() {
			let postID = $scope.postid
			let off = 0
			let lim = $scope.limit
			$http.get(`/api/actions?pid=${postID}&method=comment&limit=${lim}&offset=${off}`).then(r => r.data).then((x) => {
				let res = x.comments
				$scope.loadingComment = false
				if ($scope.cntEdit == 0) {
					if ($scope.justNewest) {
						$scope.justNewest = false
					} else {
						$scope.AcntComment = x.cntComment
						$scope.allComments = res
					}
				}
			})
		}, 5000)
		com.editMode = false
	}

	$scope.loadMoreComment = () => {
		$scope.offset += $scope.limit
		$scope.cntEdit += 1
		$scope.loadingMore = true

		let postID = $scope.postid
		let off = $scope.offset
		let lim = $scope.limit
		$http.get(`/api/actions?pid=${postID}&method=comment&limit=${lim}&offset=${off}`).then(r => r.data).then((x) => {
			let res = x.comments
			$scope.AcntComment = x.cntComment
			$scope.limit += res.length
			$scope.allComments = $scope.allComments.concat(res)
			$scope.loadingMore = false
			$scope.justNewest = true
			if (res.length < lim) {
				$scope.fullComment = true
			}
			$scope.cntEdit -= 1
		})
	}

	// ranking
	$scope.RankLoading = true;
	$scope.topten = [];
	FreeContest.getUsers((res) => {
		$scope.RankLoading = false;
		for (var i = 0; i < res.length; ++i)
			res[i].rank = i + 1;
		$scope.topten = res;
	}, 10);

	// contests
	$scope.UpcomingContests = [];
	$scope.RecentContests = [];
	$scope.PresentContest = [];
	$scope.ContestLoading = true;

	FreeContest.getContest((contests) => {
		$scope.contests = contests;
		for (var i = 0; i < contests.length; ++i) {
			let startedTime = (new Date(contests[i].startTime)).getTime();
			let endTime = (new Date(contests[i].endTime)).getTime();
			let currentTime = new Date().getTime();

			if (endTime < currentTime) {
				$scope.RecentContests.push(contests[i]);
			} else
			if (startedTime > currentTime) {
				$scope.UpcomingContests.push(contests[i]);
			} else
			if (startedTime <= currentTime && currentTime <= endTime) {
				$scope.PresentContest.push(contests[i]);
			};

			contests[i].writtersJOIN = contests[i].writters.join(', ');
		};

		$scope.RecentContests.reverse();
		$scope.ContestLoading = false;
	}, 10);
}]);

app.controller('EditContestController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', 'FreeContest', '$sce', '$routeParams', 'SweetAlert', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, FreeContest, $sce, $routeParams, SweetAlert) {
	$scope.contestid = $routeParams.id;
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;

		if (res.logined == false) {
			toastr.error('Bạn cần đăng nhập trước', 'Thông báo');
			$location.path('/login');
		};
		if (res.role !== 'admin') {
			toastr.error('Bạn không có quyền truy cập', 'Thông báo');
			$location.path('/home');
		};

		$scope.loadContest();
	});

	$scope.formLoading = false;
	$scope.contestLoading = true;

	$scope.startTime = {
		date: new Date((new Date()).toDateString())
	};
	$scope.endTime = {
		date: new Date((new Date()).toDateString())
	};

	$scope.loadContest = () => {
		FreeContest.getContestWID($scope.contestid, (contest) => {
			if (contest.length == 0 || contest.err !== undefined) {
				toastr.error(`Không tìm thấy contest với id ${$scope.contestid}`, 'Thông báo');
			} else {
				contest = contest[0];
				$scope.name = contest.name;
				$scope.duration = contest.duration;
				$scope.startTime = {
					date: new Date(contest.startTime)
				};
				$scope.endTime = {
					date: new Date(contest.endTime)
				}
				$scope.writters = contest.writters.join(',');
				$scope.id_contest = contest.id_contest;
				$scope.code = contest.code;
				$scope.practice_url = contest.practice_url;
				$scope.facebook_url = contest.facebook_url;
			};
			$scope.contestLoading = false;
		});
	};

	$scope.update = () => {
		$scope.formLoading = true;

		let cid = $scope.contestid,
			name = $scope.name,
			startTime = $scope.startTime.date,
			endTime = $scope.endTime.date,
			duration = $scope.duration,
			writters = $scope.writters.split(','),
			id_contest = $scope.id_contest,
			code = $scope.code,
			practice_url = $scope.practice_url,
			facebook_url = $scope.facebook_url;

		FreeContest.updateContest(cid, name, startTime, endTime, duration, writters, id_contest, code, practice_url, facebook_url,
		(res) => {
			if (res.err !== undefined) {
				toastr.error('Có lỗi xảy ra', 'Thông báo');
			} else {
				toastr.success('Cập nhật thành công', 'Thông báo');
				$route.reload();
			};

			$scope.formLoading = false;
		});
	};

	$scope.deleting = false;

	$scope.deleteConfirm = () => {
		SweetAlert.swal({
			title: "Chắc chắn chưa?",
			text: "_Mọi hành động nghịch ngu sẽ đều phải trả giá_",
			type: "warning",
			showCancelButton: true,
			cancelButtonText: "Thôi, nghĩ lại rồi",
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "OK, xoá luôn!",
			closeOnConfirm: false
		}, 
		function(isConfirm) {
			if (isConfirm) {
				SweetAlert.swal("OK!", "Đang tiến hành xoá contest", "success");
				$scope.delete();
			}
		});
	}

	$scope.delete = () => {
		$scope.deleting = true;

		FreeContest.deleteContest($scope.contestid, (res) => {
			if (res.err !== undefined) {
				toastr.error('Có lỗi xảy ra', 'Thông báo');
			} else {
				toastr.success('Xóa contest thành công', 'Thông báo');
				$location.path('/contests');
			};
			$scope.deleting = false;
		});
	};

	$scope.openCalendar = function(e, picker) {
		$scope[picker].open = true;
	};
}]);

app.controller('EditPostWIDController', ['$rootScope', '$scope', '$http', '$route', '$location', '$timeout', 'toastr', 'FreeContest', '$sce', '$routeParams', function ($rootScope, $scope, $http, $route, $location, $timeout, toastr, FreeContest, $sce, $routeParams) {
	$scope.postid = $routeParams.id;
	FreeContest.getSession((res) => {
		$rootScope.logined = res.logined;
		$rootScope.user = res;

		if (res.logined == false) {
			toastr.error('Bạn cần đăng nhập trước', 'Thông báo');
			$location.path('/login');
		};
		if (res.role !== 'admin') {
			toastr.error('Bạn không có quyền truy cập', 'Thông báo');
			$location.path('/home');
		};
	});

	$scope.updatePreview = () => {
		let converter = new showdown.Converter({emoji: true});
		$scope.markdownPreview = $sce.trustAsHtml(converter.makeHtml($scope.content));
	};

	$scope.formLoading = false;

	$scope.Post = {};
	FreeContest.getPostWId($routeParams.id, (res) => {
		if (res.err !== undefined || res.length == 0) {
			toastr.error('Có lỗi xảy ra', 'Thông báo');
			$location.path('/home');
		} else {
			$scope.Post = res[0];

			$scope.title = res[0].title;
			$scope.content = res[0].content;
			let converter = new showdown.Converter({emoji: true});
			$scope.markdownPreview = $sce.trustAsHtml(
				converter.makeHtml(res[0].HTMLContent)
			);
		};
	});

	$scope.update = () => {
		$scope.formLoading = true;

		let title = $scope.title,
			content = $scope.content
			pid = $scope.postid;

		FreeContest.updatePost(pid, title, content, (res) => {
			if (res.err !== undefined) {
				toastr.error('Có lỗi xảy ra', 'Thông báo');
			} else {
				toastr.success('Cập nhật bài viết thành công', 'Thông báo');
				$route.reload();
			};

			$scope.formLoading = false;
		});
	};
}]);

app.factory('FreeContest', ['$http', '$rootScope', '$timeout', function($http, $rootScope, $timeout) {
	return {
		getSession: (callback) => {
			$http.get('api/auth').then((response) => {
				callback(response.data);
			});
		},

		signIn: (username, password, callback) => {
			$http.post('api/auth', {username: username, password: password}).then(response => {
				callback(response.data);
			});
		},

		register: (name, schoolName, yob, username, password, callback) => {
			$http.put('api/auth', {
				name: name,
				schoolName: schoolName,
				yob: yob,
				username: username,
				password: password
			})
			.then((response) => {
				callback(response.data);
			});
		},

		updateWithOutP: (name, schoolName, yob, showOnRanking, callback) => {
			$http.put('api/users', {
				name: name,
				schoolName: schoolName,
				yob: yob,
				showOnRanking: showOnRanking
			}).then((response) => {
				callback(response.data);
			});
		},

		updateWithP: (name, schoolName, yob, password, showOnRanking, callback) => {
			$http.put('api/users', {
				name: name,
				schoolName: schoolName,
				yob: yob,
				password: password,
				showOnRanking: showOnRanking
			}).then((response) => {
				callback(response.data);
			});
		},

		updateAvt: (email, callback) => {
			$http.post('api/users', {
				email: email
			}).then((response) => {
				callback(response.data);
			});
		},

		getPosts: (callback) => {
			$http.get('api/posts').then((response) => {
				callback(response.data);
			});
		},

		getPostWId: (pid, callback) => {
			$http.get(`api/posts?postid=${pid}`).then((response) => {
				callback(response.data);
			});
		},

		createPost: (title, content, callback) => {
			$http.post('api/posts', {
				content: content,
				title: title
			}).then((response) => {
				callback(response.data);
			});
		},

		updatePost: (pid, title, content, callback) => {
			$http.put('api/posts', {
				pid: pid,
				content: content,
				title: title
			}).then((response) => {
				callback(response.data);
			});
		},

		deletePost: (pid, callback) => {
			$http.patch('api/posts', {
				pid: pid
			}).then((response) => {
				callback(response.data);
			});
		},

		getUser: (username, callback) => {
			$http.get(`api/users?username=${username}`).then((response) => {
				callback(response.data);
			});
		},

		getUsers: (callback, limit = 99999, offset = 0) => {
			$http.get(`api/users?limit=${limit}&offset=${offset}`).then((response) => {
				callback(response.data);
			});
		},

		getContest: (callback, limit = 99999) => {
			$http.get(`api/contest?limit=${limit}`).then((response) => {
				callback(response.data);
			});
		},

		getContestX: (uid, callback, limit = 99999) => {
			$http.get(`api/contest?uid=${uid}&limit=${limit}`).then((response) => {
				callback(response.data);
			});
		},

		getContestWID: (id, callback) => {
			$http.get(`api/contest?cid=${id}`).then((response) => {
				callback(response.data);
			});
		},

		updateContest: (cid, name, startTime, endTime, duration, writters, id_contest, code, practice_url, facebook_url, callback) => {
			$http.patch('api/contest', {
				cid: cid,
				name: name,
				startTime: startTime,
				endTime: endTime,
				duration: duration,
				writters: writters,
				id_contest: id_contest,
				code: code,
				practice_url: practice_url,
				facebook_url, facebook_url
			}).then((response) => {
				callback(response.data);
			});
		},

		addContest: (name, startTime, endTime, duration, writters, callback) => {
			$http.put('api/contest', {
				name: name,
				startTime: startTime,
				endTime: endTime,
				duration: duration,
				writters: writters
			}).then((response) => {
				callback(response.data);
			});
		},

		deleteContest: (cid, callback) => {
			$http.post('api/contest', { cid: cid })
			.then((response) => {
				callback(response.data);
			});
		},

		registerContest: (cid, callback) => {
			$http.put('api/contest/register', { cid: cid })
			.then((response) => {
				callback(response.data);
			});
		},

		getUsersRegistered: (cid, callback) => {
			$http.post('api/contest/register', { cid: cid })
			.then((response) => {
				callback(response.data);
			});
		},

		getTmpPassword: (uid, cid, callback) => {
			$http.post('/api/tmpPassword', { uid: uid, cid: cid })
			.then((response) => {
				callback(response.data)
			})
		},

		getSingleContest: (params, callback) => {
			$http.post('/api/singleContest', params).then((res) => {
				callback(res.data);
			});
		},

		regSingleContest: (params, callback) => {
			$http.post('/api/regSingleContest', params).then((res) => {
				callback(res.data);
			});
		},

		logOut: (callback) => {
			$http.delete('api/auth').then((response) => {
				callback(response.data);
			});
		}
	};
}]);
