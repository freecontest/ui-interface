'use strict';

const User = require('./db/user');
const Post = require('./db/posts');
const Contest = require('./db/contest');
const Comments = require('./db/comments');
const sequelize = require('./db/default');
const tmpPassword = require('./db/tmpPassword');
const Registration = require('./db/registration');
const logsUpdateRating = require('./db/logsUpdateRating')

async function setup() {
    // await Comments.sync({force: true});
    // await Contest.sync({force: true});
    // await logsUpdateRating.sync({force: true});
    // await Post.sync({force: true});
    // await Registration.sync({force: true});
    // await User.sync({force: true});
    await tmpPassword.sync({force: true});
    return console.log('OK')
}

async function addAdmin(username) {
    await User.update({
    	role: 'admin',
        color: 0
    }, {
    	where: { username: username }
    });
    return console.log(`Updated ${username} role to admin`)
}

async function run() {
    const args = process.argv;
    if (args[2] === 'setup') return await setup()
    if (args[2] === 'addAdmin') {
        let username = args[3] || null
        return (username === null
                ? console.log('where username??')
                : await addAdmin(username)
        )
    }

    return console.log(`nah nah`)
}

run()
